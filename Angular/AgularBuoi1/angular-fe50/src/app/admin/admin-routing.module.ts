import { AdminComponent } from './admin.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MoviesComponent} from './movies/movies.component';
import {UsersComponent} from './users/users.component'


const routes: Routes = [
{ path:'',component:AdminComponent,children:[
  // nếu pass là trổng thi tư đong chuyển sang admin/user
  {path:'',redirectTo:"users"},
  {path:'users',component:UsersComponent},
  {path:'movies',component:MoviesComponent}
]}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
