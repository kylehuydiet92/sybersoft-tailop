import { AdminGuard } from './../core/guards/admin.guard';

import { BaiTapDatGheComponent } from './bai-tap-dat-ve/bai-tap-dat-ghe/bai-tap-dat-ghe.component';
import { MovieComponent } from './movie/movie.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Baitap2Component} from './baitap2/baitap2.component'
// import {MainModule} from './main/main.module'
// import { AdminModule } from './admin/admin.module';


const routes: Routes = [
  //cách : Routing theo componet
  // {path:"baitap2",component:Baitap2Component},
  // {path:"movie",component:MovieComponent},
  // {path:"baitapdatve",component:BaiTapDatGheComponent},
  //cách 2 : routing theo module
  // pathMatch:"full" tương đương vs exact bên tract 
  // {path:"",pathMatch:"full" ,loadChildren:() =>  MainModule}
  //
  {
    
    path:"admin" ,canActivate:[AdminGuard]  ,loadChildren:() => import('./admin/admin.module').then((m)=>m.AdminModule)},
  {path:"",loadChildren:() => import('./main/main.module').then((m)=>m.MainModule)},
  {path:"",loadChildren:() => import('./auth/auth.module').then((m)=>m.AuthModule)}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
