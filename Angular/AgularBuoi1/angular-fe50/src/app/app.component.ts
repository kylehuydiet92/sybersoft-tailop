import { AuthService } from 'src/core/service/auth.service';
import { Component,OnInit } from '@angular/core';

// meta-data
@Component({
  selector: 'app-root',//ten compent đẻ nhúng vài html
  templateUrl: './app.component.html',//link tới file html
  styleUrls: ['./app.component.scss']//link tới file style
})
export class AppComponent {
  constructor(private auth:AuthService){

  }

  ngOnInit():void{
    this.auth.initCurrentUser();
  }
  title = 'angular-fe50';
}
