import { AuthInterceptor } from './../core/interceptors/auth.interceptor';

import { PipeModule } from './shared/pipe/pipe.module';
import { BaiTapDatVeModule } from './bai-tap-dat-ve/bai-tap-dat-ve.module';
import { ProductModule } from './product/product.module';
import { MovieModule } from './movie/movie.module';
import { DirectivesModule } from './directives/directives.module';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule,HTTP_INTERCEPTORS} from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DemoComponent } from './demo/demo.component';
import {Baitap1Module} from './baitap1/baitap1.module';
import {Baitap2Module} from './baitap2/baitap2.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { DataBindingModule } from './data-binding/data-binding.module';

//ngMOdule là một 1 metadata : khai báo cho angurla biết class apmodule là 1 module
@NgModule({
  //Nơi khahi báo các component để  module quản lý 
  // 1 component bắt buôc phải được quản lý bời 1 module
  // 1 module có thể quản lý nhiều component 
  declarations: [
    AppComponent,
    DemoComponent,

    

  ],
  // import
  //vd: có HomeModule có : homepageCompont,DelailComponent
  //chỉ cần import HomeMOdule vào AppModule là có thể sử dụng 
  //tates cả component của HomeMOduleư
  //MOdule cảu angular : HTTPClientModule,FormsModule,RouterModule

  imports: [
    BrowserModule,
    AppRoutingModule,
    // Baitap1Module,
    // Baitap2Module,
    // DirectivesModule,
    // MovieModule,
    // ProductModule,
    // BaiTapDatVeModule,
    HttpClientModule,
    BrowserAnimationsModule,

 
  ],
  // import services
  providers: [{provide:HTTP_INTERCEPTORS,useClass:AuthInterceptor,multi:true}],
  // khai báo appCOmponet là component khơi chạy đầu tiên
  //chỉ khởi tạo ở appmodule sau này k cần tạo ra
  bootstrap: [AppComponent]
})
export class AppModule { }
