import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl,Validators} from '@angular/forms'
import {AuthService} from 'src/core/service/auth.service'
import {Router} from '@angular/router'

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  signForm:FormGroup;

  constructor(private auth:AuthService,private router:Router) {
    this.signForm= new FormGroup({
      taiKhoan : new FormControl("",([Validators.required])),
      matKhau : new FormControl("",([Validators.required,Validators.minLength(3),Validators.maxLength(20)]))
    });
   }

  ngOnInit(): void {

  }
  handleSignIn(){
    // đáng dauas tất cả input trang thai touched là true
    //nếu k hợp lệ kết thúc
    this.signForm.markAllAsTouched()
   if( this.signForm.invalid) return
  
   this.auth.signin(this.signForm.value).subscribe({
     next:result =>{
       console.log(result);
       localStorage.setItem('user',JSON.stringify(result));
        // cập nhâp thông tin user vào biên cureenUSer trong authService
        this.auth.currenUser.next(result);
        this.router.navigate(['/'])
        
       
     },
     error:(err) =>{
       console.log(err.error);
       
     }
   })
  }
}
