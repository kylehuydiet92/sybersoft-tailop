import { Component, OnInit,ViewChild,} from '@angular/core';
import {AuthService} from 'src/core/service/auth.service';
import {Router} from '@angular/router'
import {NgForm} from '@angular/forms'
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
@ViewChild('signupForm') signupForm:NgForm;
  constructor(private auth:AuthService,private router:Router) { }

  
  ngOnInit(): void {
  }

  isDirtyForm():boolean{
    return this.signupForm.dirty && !this.signupForm.submitted
  }
  handleSignUp(value){
    this.auth.signup(value).subscribe({
      next:result =>{
        // điều hướng trong component
        this.router.navigate(['/signin']);
      },
      error:(err)=>{
        console.log(err.error);
        
      }
    });

     
  }

}
