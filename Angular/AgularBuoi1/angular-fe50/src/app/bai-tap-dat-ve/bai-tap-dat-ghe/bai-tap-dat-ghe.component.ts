import { GheComponent } from './../ghe/ghe.component';
import { Ghe } from './../../models/ghe';
import { Component, OnInit,ViewChildren,QueryList } from '@angular/core';

@Component({
  selector: 'app-bai-tap-dat-ghe',
  templateUrl: './bai-tap-dat-ghe.component.html',
  styleUrls: ['./bai-tap-dat-ghe.component.scss'],
})
export class BaiTapDatGheComponent implements OnInit {
  mangGhe: Ghe[] = [
    { SoGhe: 1, TenGhe: ' số 1 ', Gia: 100, TrangThai: false },
    { SoGhe: 2, TenGhe: ' số 2 ', Gia: 100, TrangThai: false },
    { SoGhe: 3, TenGhe: ' số 3 ', Gia: 100, TrangThai: false },
    { SoGhe: 4, TenGhe: ' số 4 ', Gia: 100, TrangThai: false },
    { SoGhe: 5, TenGhe: ' số 5 ', Gia: 100, TrangThai: false },
    { SoGhe: 6, TenGhe: ' số 6 ', Gia: 100, TrangThai: false },
    { SoGhe: 7, TenGhe: ' số 7 ', Gia: 100, TrangThai: false },
    { SoGhe: 8, TenGhe: ' số 7 ', Gia: 100, TrangThai: false },
    { SoGhe: 9, TenGhe: ' số 9 ', Gia: 100, TrangThai: false },
    { SoGhe: 10, TenGhe: ' số 10 ', Gia: 100, TrangThai: false },
    { SoGhe: 11, TenGhe: ' số 11 ', Gia: 100, TrangThai: false },
    { SoGhe: 12, TenGhe: ' số 12 ', Gia: 100, TrangThai: false },
    { SoGhe: 13, TenGhe: ' số 13 ', Gia: 100, TrangThai: false },
    { SoGhe: 14, TenGhe: ' số 14 ', Gia: 100, TrangThai: false },
    { SoGhe: 15, TenGhe: ' số 15 ', Gia: 100, TrangThai: false },
    { SoGhe: 16, TenGhe: ' số 16 ', Gia: 100, TrangThai: false },
    { SoGhe: 17, TenGhe: ' số 17 ', Gia: 100, TrangThai: false },
    { SoGhe: 18, TenGhe: ' số 18 ', Gia: 100, TrangThai: false },
    { SoGhe: 19, TenGhe: ' số 19 ', Gia: 100, TrangThai: false },
    { SoGhe: 20, TenGhe: ' số 20 ', Gia: 100, TrangThai: false },
    { SoGhe: 21, TenGhe: ' số 21 ', Gia: 100, TrangThai: false },
    { SoGhe: 22, TenGhe: ' số 22 ', Gia: 100, TrangThai: false },
    { SoGhe: 23, TenGhe: ' số 23 ', Gia: 100, TrangThai: false },
    { SoGhe: 24, TenGhe: ' số 24 ', Gia: 100, TrangThai: false },
    { SoGhe: 25, TenGhe: ' số 25 ', Gia: 100, TrangThai: false },
    { SoGhe: 26, TenGhe: ' số 26 ', Gia: 100, TrangThai: false },
    { SoGhe: 27, TenGhe: ' số 27 ', Gia: 100, TrangThai: false },
    { SoGhe: 28, TenGhe: ' số 28 ', Gia: 100, TrangThai: false },
    { SoGhe: 29, TenGhe: ' số 29 ', Gia: 100, TrangThai: false },
    { SoGhe: 30, TenGhe: ' số 30 ', Gia: 100, TrangThai: true },
    { SoGhe: 31, TenGhe: ' số 31 ', Gia: 100, TrangThai: false },
    { SoGhe: 32, TenGhe: ' số 32 ', Gia: 100, TrangThai: false },
    { SoGhe: 33, TenGhe: ' số 33 ', Gia: 100, TrangThai: false },
    { SoGhe: 34, TenGhe: ' số 34 ', Gia: 100, TrangThai: false },
    { SoGhe: 35, TenGhe: ' số 35 ', Gia: 100, TrangThai: false },
  ];

  mangGheDangDat: Ghe[] = [];
  constructor() {}

  ///dùng viewChuldren để dom đến các tag <app-ghe></app-ghe>
  @ViewChildren(GheComponent) dsTagAppGhe:QueryList<GheComponent>;

  ngOnInit(): void {}
  tinhTongTien():number{
     const tt:number =  this.mangGheDangDat.reduce((tongtien:number,gheDangDat:Ghe,index:number)=>{
      return  tongtien += gheDangDat.Gia;
    },0)
    return tt;
  };


  datGhe(gheDuocClick): void {
    console.log(gheDuocClick);
    //ktra khi click vao ghe, ghe đó có trang mang chưa ? nếu có thì xóa neeuschwa có thì thêm vào
    let index = this.mangGheDangDat.findIndex(
      (gheDangDat) => gheDangDat.SoGhe === gheDuocClick.SoGhe
    );

    if (index !== -1) {
      this.mangGheDangDat.splice(index, 1);
    } else {
      this.mangGheDangDat.push(gheDuocClick);
    }
    //sort
    this.mangGheDangDat.sort((item2, item1) => {
      if (item2.SoGhe < item1.SoGhe) {
        return -1;
      }
      return 1;
    });

   
  }

  huyGhe(soGhe:number):void{
      // console.log("so ghe huy",soGhe);
      this.mangGheDangDat = this.mangGheDangDat.filter(gheDangDat => gheDangDat.SoGhe !== soGhe);
      let mangTagAppGhe = this.dsTagAppGhe.toArray();

      for(let index = 0;index<mangTagAppGhe.length;index++){
        //mỗi lân lấy ra môt thẻ app ghe
        let appGhe:GheComponent = mangTagAppGhe[index];
        if(appGhe.inputGhe.SoGhe === soGhe){
          appGhe.dangDat = false;
        }

        
      }
      
  }
}
