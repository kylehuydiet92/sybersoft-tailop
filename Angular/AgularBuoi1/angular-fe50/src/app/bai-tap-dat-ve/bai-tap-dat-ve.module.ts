import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GheComponent } from './ghe/ghe.component';
import { BaiTapDatGheComponent } from './bai-tap-dat-ghe/bai-tap-dat-ghe.component';



@NgModule({
  declarations: [GheComponent, BaiTapDatGheComponent],
  imports: [
    CommonModule
  ],
  exports:[BaiTapDatGheComponent]
})
export class BaiTapDatVeModule { }
