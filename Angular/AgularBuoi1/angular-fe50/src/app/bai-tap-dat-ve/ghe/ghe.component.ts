import { Ghe } from './../../models/ghe';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-ghe',
  templateUrl: './ghe.component.html',
  styleUrls: ['./ghe.component.scss'],
})
export class GheComponent implements OnInit {
  @Input() inputGhe: Ghe = new Ghe();
  //thuốc tính quy định ghế dang được người dùng chọn
  @Output() eventDatGhe = new EventEmitter();//đưa giá trị ra ngoài compont nào sữ dụng
  dangDat:boolean = false;

  datGhe():void{
    this.dangDat = !this.dangDat;
    //dùng eventDatGhe đưa giá trị ghê dược click ra ngoài
    
    this.eventDatGhe.emit(this.inputGhe);
  }
  constructor() {}

  ngOnInit(): void {}
}
