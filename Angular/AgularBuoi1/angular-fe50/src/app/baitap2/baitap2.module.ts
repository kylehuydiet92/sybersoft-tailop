import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { Baitap2Component } from './baitap2.component';
import { FooterComponent } from './footer/footer.component';
import { IndexComponent } from './index/index.component';
import { IndexcontentComponent } from './indexcontent/indexcontent.component';
import { SliderComponent } from './slider/slider.component';
import { ItemComponent } from './item/item.component';



@NgModule({
  declarations: [HeaderComponent, Baitap2Component, FooterComponent, IndexComponent, IndexcontentComponent, SliderComponent, ItemComponent],
  imports: [
    CommonModule
  ],
  exports:[
    Baitap2Component
  ]
})
export class Baitap2Module { }
