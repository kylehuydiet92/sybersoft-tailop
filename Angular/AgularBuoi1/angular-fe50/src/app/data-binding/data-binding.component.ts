import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.scss']
})
export class DataBindingComponent implements OnInit {
  messages:string ="hello Mạnh";
  isDisabled:boolean = false;
  fullName:string=" ";
  userName:string=" ";

  email:string ="";

  address :string = "";

  changeFullName(event){
    this.fullName = event.target.value;
  }

  // changeEmail(){
  // this.email=(<HTMLInputElement> document.getElementById("email")).value;
 
  // }

  changeEmail(emailRef){
    console.log(emailRef);
    this.email = emailRef.value;
    
  }
  constructor() { }

  ngOnInit(): void {
  }

}
