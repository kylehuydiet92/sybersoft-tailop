import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-attribute-directives',
  templateUrl: './attribute-directives.component.html',
  styleUrls: ['./attribute-directives.component.scss']
})
export class AttributeDirectivesComponent implements OnInit {

  isAcctive:boolean=true;
  mesage:string = "hello";
  data:String = ` <span class="text-danger">Error</span> `
  imgUrl:string="https://9mobi.vn/cf/images/2015/03/nkk/hinh-dep-1.jpg"
  constructor() { }

  ngOnInit(): void {
  }

}
