import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StructuraldiretivesComponent } from './structuraldiretives/structuraldiretives.component';
import { ButtonComponent } from './button/button.component';
import { CartComponent } from './cart/cart.component';
import { AttributeDirectivesComponent } from './attribute-directives/attribute-directives.component';
import { HightlightDirective } from './hightlight.directive';



@NgModule({
  declarations: [StructuraldiretivesComponent, ButtonComponent, CartComponent, AttributeDirectivesComponent, HightlightDirective],
  imports: [
    CommonModule
  ],
  exports:[StructuraldiretivesComponent,AttributeDirectivesComponent]
})
export class DirectivesModule { }
