import { Directive,ElementRef,HostListener } from '@angular/core';

@Directive({
  selector: '[appHightlight]'
})
export class HightlightDirective {

  constructor(private elementRef:ElementRef) 
  {
    // elementRef đại diện cho cái tag 
    this.elementRef.nativeElement.style.backgroundColor="red"
    this.elementRef.nativeElement.classList.add('w-25')

   }

   @HostListener('mouseover') handleMouseover(){
    this.elementRef.nativeElement.style.backgroundColor="blue"
   }
  //  @HostListener('click') handleClick(){
    
  //  }

   @HostListener('mouseleave') handleMouseleave(){
    this.elementRef.nativeElement.style.backgroundColor="pink"
   }

}
