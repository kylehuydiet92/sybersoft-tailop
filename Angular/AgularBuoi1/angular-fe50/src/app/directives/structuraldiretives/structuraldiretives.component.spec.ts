import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StructuraldiretivesComponent } from './structuraldiretives.component';

describe('StructuraldiretivesComponent', () => {
  let component: StructuraldiretivesComponent;
  let fixture: ComponentFixture<StructuraldiretivesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StructuraldiretivesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StructuraldiretivesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
