import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-structuraldiretives',
  templateUrl: './structuraldiretives.component.html',
  styleUrls: ['./structuraldiretives.component.scss']
})
export class StructuraldiretivesComponent implements OnInit {
  isActive:boolean = false;
  isLoggedIn:boolean = false;
  color:string = "red";
  dshv:any=[
    {hoten:"nguyễn đức mạnh",lop:"FE50"},
    {hoten:"nguyễn đức thanh",lop:"FE50"},
    {hoten:"nguyễn đức thái",lop:"FE50"},
    {hoten:"nguyễn đức quang",lop:"FE50"},
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
