import { Component, OnInit } from '@angular/core';
//dùng để tương tác với params
import {ActivatedRoute} from '@angular/router'
import {MovieService} from 'src/core/service/movie.service'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  movieDetail:any = null;
  loading:boolean=false;
  error:string = "";
  // demo
  obj:any = {
    hoTen:'Nguyen',
    lop:'fe50',
    greder:'man',
    age:25
  }
  constructor(private activatedRoute:ActivatedRoute,private movieService:MovieService) { }

  ngOnInit(): void {
    // lay param
    this.activatedRoute.params.subscribe({
      next:(params) =>{
        console.log("params :" , params);
        //láy được maxphim ta param,dung mã phim gọi api
        this.loading = true
        
        this.movieService.getMovieDetail(params.id).subscribe({
          next:result => {
            this.movieDetail =result;
            this.loading = false
            console.log(this.movieDetail);
            
          },
          error: error=>{
            console.log(error);
            this.loading = false;
            this.error = error.error;

          }
        })
      }
    })
    //queryparams 
    this.activatedRoute.queryParams.subscribe({
      next:params=>{
        console.log(params)
      }
    })
  };

}
