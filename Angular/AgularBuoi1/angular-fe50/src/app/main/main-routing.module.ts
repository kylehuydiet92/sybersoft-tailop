import { MainComponent } from './main.component';
import { MovieComponent } from './movie/movie.component';
import { BookingComponent } from './booking/booking.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:"",component:MainComponent,children:[
    {path:"home/:id",component:HomeComponent},
    {path:"home",component:HomeComponent},
    {path:"movie",component:MovieComponent},
  ]},

  {path:"booking",component:BookingComponent},

];

@NgModule({
  //ngoai trư app-routing module là sử dụng forRoot, còn lại tất cả modole khác là forchild
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
