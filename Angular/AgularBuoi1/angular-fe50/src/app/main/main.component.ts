import { AuthService } from 'src/core/service/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
currentUser:any;

  constructor(private auth:AuthService) { }

  ngOnInit(): void {
    //chỉ lấy value crrenUser đúng 1 lần duy nhất
    //nếu có trường hop giá trị cảu currenUser nó được cạp nhâp thi cần phải goi lai jthif
    // this.auth.curentUser.vaue 1 lân nữa 
    //this.currentUser = this.auth.currenUser.value
    //tự động dõi gia tri của curenUSer trong AuthSerVice
    //khi currenUser thay đổi gái trị => tự dộng nhảy vào callback next
    this.auth.currenUser.asObservable().subscribe({
      next:(result) =>{
        this.currentUser =result;
      }
    })
  }

}
