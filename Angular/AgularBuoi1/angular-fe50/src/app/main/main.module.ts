import { PipeModule } from './../shared/pipe/pipe.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { HomeComponent } from './home/home.component';
import { MovieComponent } from './movie/movie.component';
import { BookingComponent } from './booking/booking.component';
import { MainComponent } from './main.component';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [
    HomeComponent,
    MovieComponent,
    BookingComponent,
    MainComponent,
  ],
  imports: [CommonModule, MainRoutingModule, PipeModule, MatButtonModule],
})
export class MainModule {}
