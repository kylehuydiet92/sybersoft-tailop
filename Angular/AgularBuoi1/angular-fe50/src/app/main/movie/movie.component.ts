import { Movie, MovieInterFace, MovieType } from './../../../core/model/movie';
import { Component, OnInit } from '@angular/core';
import {MovieService} from 'src/core/service/movie.service'

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
  movieList:Movie[]=[]
  //phải khai báo nó trong contrustor
  constructor(private movieService:MovieService) { }


  //ngOnInit <=> tương đương vs compoont dismount
  //thích hợp vs goi api tương tac vs Dom setTimeOut
   ngOnInit(): void {
     //tách biệt vs logic khỏi componet,compont chỉ viết goi tới cá hàm trong service,l
    //  this.movieList = this.movieService.getMovieList()
    // this.movieService.getMovieListPromise().then(res=>{
    //   this.movieList = res as any
    // }).catch((error)=>console.log(error)
    // )

    this.movieService.getMovieList().subscribe({
      //next:ket quả
      next:(result) =>{
        this.movieList = result
        console.log(this.movieList);
        
      },
      // lỗi
      error:(error)=>{
        console.log(error);
        
      },//compete chay sau khi thanh công
      complete:()=>{
        console.log('done');  
        
      }
    })

  }

}
