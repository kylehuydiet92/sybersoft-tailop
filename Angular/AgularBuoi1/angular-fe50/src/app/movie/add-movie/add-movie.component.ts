import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.scss']
})
export class AddMovieComponent implements OnInit {
  @Output() onAdd = new EventEmitter();

  constructor() { }

  handleAdd(name,price,image){




  //  console.log(name,price,image);
   //từ file to base64(dạng mã hóa) để có thể đưa vao src của tag img
   const fileReader = new FileReader()
   fileReader.readAsDataURL(image);
   fileReader.onload=(event) =>{
    const movie = {
      name,
      price,
      image:event.target.result,
    }
    // console.log(movie);
    this.onAdd.emit(movie);
    
   }

   
    
  }
  ngOnInit(): void {
  }

}
