import { Component, Input, OnInit,EventEmitter,Output} from '@angular/core';
 

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.scss']
})
export class MovieItemComponent implements OnInit {
  //khai báo Input dể nhận dwxx liêu từ componet cha
  @Input() movie:any;

  //EvenEmitter là một dooidstuong customer event
  @Output() onDelete = new EventEmitter()
  constructor() { }

  handleDelete(){
    //đẩy bô phim muốn xóa lên compont cha
    // trong angular,muốn đây dữ liêu từ con lên cha thì phải thông qua 1 cái event
    //=??? event ở đâu
    //triger event,đẩy event lên componet cha
    //this.movive <=> $event
    this.onDelete.emit(this.movie)
  }  
  ngOnInit(): void {
  }

}
