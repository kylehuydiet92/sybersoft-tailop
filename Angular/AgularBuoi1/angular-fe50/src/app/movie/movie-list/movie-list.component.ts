import { Component, OnInit,Input,OnChanges } from '@angular/core';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit,OnChanges {

  @Input() movieAdd:any;
  

  movieList:any[]=[
    {name:'iron man',price:12000,image:'assets/image/movie1.jpg'},
    {name:'naruto',price:22000,image:'assets/image/movie2.jpeg'},
    {name:'one pice',price:16000,image:'assets/image/movie3.png'},
  ]
  constructor() { }


  //một lifecycle hook: chạy khi input bị thay đổi
  ngOnChanges(changes){
    console.log(changes);

    const { movieAdd } = changes;
    if(movieAdd?.currentValue?.name !== movieAdd?.previousValue?.name){
      this.movieList.push(movieAdd.currentValue);
    }
    
  }

  handleDelete(movie){
    // console.log("movei",movie);
    this.movieList= this.movieList.filter((item)=>{
      return item.name !== movie.name;
    })
    
  }
  ngOnInit(): void {
  }

}
