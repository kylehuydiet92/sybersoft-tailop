import { MovieListComponent } from './movie-list/movie-list.component';
import { Component, OnInit,ViewChild } from '@angular/core';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
  @ViewChild('movieListComponent') movieListComponent:MovieListComponent

  movieAdd:any;

  constructor() { }

  handleAdd(movie){
    // this.movieAdd=movie
    this.movieListComponent.movieList.push(movie)
  }
  ngOnInit(): void {
  }

}
