import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cartList:any[] = [];
  constructor() { }

  handleAddToCart(prodcut){
    const index = this.cartList.findIndex((item)=>item.id===prodcut.id)
    //cjwa ton tai trong giỏ hàng
    if (index === -1) { 
      this.cartList.push({...prodcut,quality:1})
    }else{
      this.cartList[index].quality +=1;
    }

    console.log(this.cartList);
    
  }

 
  ngOnInit(): void {
  }

}
