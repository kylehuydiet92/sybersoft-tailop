import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
@Output() onAddToCart = new EventEmitter();
  productList:any[]=[
    {id:1,name:"iphone12",price:1212313,image:'assets/image/product1.jpg'},
    {id:2,name:"iphone10",price:100003,image:'assets/image/product2.jpeg'},
    {id:3,name:"iphoneX",price:1212313,image:'assets/image/product3.png'}
  ]

  constructor() { }

  handleAddToCart(product){
    this.onAddToCart.emit(product)
  }
  ngOnInit(): void {
  }

}
