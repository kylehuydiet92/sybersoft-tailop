
import { Component, OnInit,ViewChild } from '@angular/core';
import { CartComponent } from './cart/cart.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  @ViewChild('cartComponent') cartComponent:CartComponent
  constructor() { }

  handleAddToCart(product){
  //viet ham handleAddToCart ở componet procut
  //sử dụng view child đẻ gọi hàm đó
    this.cartComponent.handleAddToCart(product)
    
  }
  ngOnInit(): void {
  }

}
