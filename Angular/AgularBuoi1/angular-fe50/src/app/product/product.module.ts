import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { CartComponent } from './cart/cart.component';



@NgModule({
  declarations: [ProductComponent, ProductListComponent, ProductItemComponent, CartComponent],
  imports: [
    CommonModule
  ],
  exports:[ProductComponent]
})
export class ProductModule { }
