import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorsMesComponent } from './errors-mes/errors-mes.component';



@NgModule({
  declarations: [ErrorsMesComponent],
  imports: [
    CommonModule
  ],
  exports:[ErrorsMesComponent]
})
export class ComponentsModule { }
