import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorsMesComponent } from './errors-mes.component';

describe('ErrorsMesComponent', () => {
  let component: ErrorsMesComponent;
  let fixture: ComponentFixture<ErrorsMesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ErrorsMesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorsMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
