import { FormControl } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-errors-mes',
  templateUrl: './errors-mes.component.html',
  styleUrls: ['./errors-mes.component.scss']
})
//<app-errors-mes validator="signinForm.get('taiKhoan')"> </app-errors-mes>
// name ="taiKhoan"
export class ErrorsMesComponent implements OnInit {
@Input() name:string;
@Input() validator:FormControl;

  messages:any = {
    taiKhoan:{
      required:"Tài Khoản không được đẻ trống"
    },
    matKhau:{
      required:"Mật Khẩu không được đẻ trống",
      minlength:"Mật khẩu không được ít hơn 3 kí tự",
      maxlength:"Mật khẩu không được nhiều hơn 20 kí tự"
    },
    email:{
      required:"Email không được đẻ trống",
      pattern:"Email không đúng định dạng"
    },
    hoTen:{
      required:"Họ tên không được đẻ trống",
    },
    soDt:{
      required:"Số điện thoại không được đẻ trống",
    }
  }
  constructor() { }

  ngOnInit(): void {
  }

}
