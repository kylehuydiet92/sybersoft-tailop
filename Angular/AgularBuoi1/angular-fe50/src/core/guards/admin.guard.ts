import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,Router
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(private router:Router){

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    // return vê true cho phép truy cập vào

    // lấy thông tin trong localStorage
    const user = localStorage.getItem('user')
    if(user){
      const {maLoaiNguoiDung} = JSON.parse(user)
      if(maLoaiNguoiDung === 'QuanTri'){
        //đã đăng nhâp có quyền addmin => cho phep truy cập
        return  true;
      }

      //đã đằng nhập k có quyền admin -> redirect về rang Home
      this.router.navigate(['/movie']);
      return false
    }
    //chưa đăng nhập về redirect về trang sign
    this.router.navigate(['/signin']);

    return false;
  }
}
