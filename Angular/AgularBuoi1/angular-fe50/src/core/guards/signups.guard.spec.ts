import { TestBed } from '@angular/core/testing';

import { SignupsGuard } from './signups.guard';

describe('SignupsGuard', () => {
  let guard: SignupsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(SignupsGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
