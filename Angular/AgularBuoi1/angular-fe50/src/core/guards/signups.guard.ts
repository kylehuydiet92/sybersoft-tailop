import { SignupComponent } from './../../app/auth/signup/signup.component';
import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SignupsGuard implements CanDeactivate<SignupComponent> {
  canDeactivate(
    component: SignupComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      // return về true => cho phép ra khỏ page
      const isDirty =  component.isDirtyForm();
      //form đã bị thay đổi,cần hiện thông báo khi người dùng rời khỏi
      if(isDirty){
        return  confirm('Bạn có chắc muốn rời khỏi ... ')
      }
    return true;
  }
  
}
