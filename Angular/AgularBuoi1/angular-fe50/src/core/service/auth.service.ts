import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  //đối tượng behaviorsubject đê lưu trư 1 gobal variable
  currenUser = new BehaviorSubject(null);
  //thí.currenUser.next(value): set lại giá trị cho biến currentUser
  // this.currenUser.value:get giá tri của biên currenUser tại 1 thời điểm
  //this.currenUser.asObservable(): chuyển currenUser thành Observable()=> có thẻ subscribe của BehaviorSubject
  // this.currenUser.asObservable().subcribe()=>theo doi sự thây dodori của value currenUser

  constructor(private http: HttpClient) {}
  //kiểm tra trong localStorage có thông tin User hay k thì sẻ set lại vao biên currentUser
  initCurrentUser(){
    const user = localStorage.getItem('user');
    if(user){
      this.currenUser.next(JSON.parse(user))
    }
  }


  signup(values: any): Observable<any> {
    const url = 'https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy';
    return this.http.post(url, { ...values, maNhom: 'GP01' });
  }

  signin(values: any): Observable<any> {
    const url =
      'https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap';
    return this.http.post(url, values);
  }
}
