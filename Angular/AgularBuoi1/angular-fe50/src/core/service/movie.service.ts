import { Movie, MovieInterFace, MovieType } from './../model/movie';
import { Injectable ,} from '@angular/core';
import {Observable, observable} from 'rxjs'
import {HttpClient,HttpParams,HttpHeaders} from '@angular/common/http'
//@ meta data
@Injectable({
  // Từ phiên bản 5 trở lên mới có cái key   providedIn: 'root'  thì k cần import vao appmodule nữa

  providedIn: 'root',
})
export class MovieService {
  constructor(private http:HttpClient) {


  }

  getMovieList():Observable<Movie[]>{
    const url = 'https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim';

    let params = new HttpParams();
    params =  params.append('maNhom','GP01');
    return this.http.get<Movie[]>(url,{params});
  }

  getMovieDetail(id:string):Observable<any>{
    const url ="https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim"
    let params = new HttpParams();
    params =params.append('maPhim',id);
    return this.http.get(url,{params})
  }










  // getMovieList():MovieType[] {
  //   return [
  //     { name: 'iron man', price: 12000, image: 'assets/image/movie1.jpg' },
  //     { name: 'naruto', price: 22000, image: 'assets/image/movie2.jpeg' },
  //     { name: 'one pice', price: 16000, image: 'assets/image/movie3.png' },
  //   ];
  // }

  // getMovieListPromise(){
  //   return   new Promise((resolve,reject)=>{
  //     //fake goi api khoảng 2s 
  //     setTimeout(()=>{
     
  //       //thành công trả về kết quả
  //       //  resolve(
  //       //   [
  //       //     { name: 'iron man', price: 12000, image: 'assets/image/movie1.jpg' },
  //       //     { name: 'naruto', price: 22000, image: 'assets/image/movie2.jpeg' },
  //       //     { name: 'one pice', price: 16000, image: 'assets/image/movie3.png' },
  //       //   ]
  //       // )

  //          // thất bại trả về lỗi
  //          reject('error')

  //     },2000)
  //   })
  // }


  // // observable
  // getMovieListObservable(){
  // return new Observable(subscribe=>{
  //   setTimeout(() => {
  //     //subscribe.next() là emit dữ liệu
  //     //subscribe.next()  sẻ k kết thuc obrivable 
  //     subscribe.next([
  //       { name: 'iron man', price: 12000, image: 'assets/image/movie1.jpg' },
      
       
  //     ])
  //   }, 2000);


  //   setTimeout(() => {
          

  //     subscribe.next([
       
  //       { name: 'naruto', price: 22000, image: 'assets/image/movie2.jpeg' },
    
  //     ])        //error 
  //     subscribe.error('netwword error') //kết thúc observable
  //     ;subscribe.complete() //sau khi comlete thì sub.next k chay nữa kết thúc
  //   }, 4000);

  
    

  // })
  // }
}
