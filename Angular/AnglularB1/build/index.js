var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
console.log('Hello Typescript');
// var vs let
// scope : var  = function scope, let block scope
// hoisting:
// console.log(a);
// var a = 5;
// console.log(b);
// let b = 5
for (var i = 0; i < 5; i++) {
    setTimeout(function () {
        console.log(i);
    }, 1000);
}
var _loop_1 = function (i_1) {
    setTimeout(function () {
        console.log(i_1);
    }, 1000);
};
for (var i_1 = 0; i_1 < 5; i_1++) {
    _loop_1(i_1);
}
// IIFE function :là function đc khơi tạo và chạy ngay khi khởi tạo
// function loop(i)
for (var i = 0; i < 5; i++) {
    (function (i) {
        setTimeout(function () {
            console.log(i);
        }, 1000);
    })(i);
}
// khai báo kiểu dữ liệu
var messages = 'hello';
// messages = 5
var diemToan = 10;
// diemToan = "sda"
var isActive = true;
// isActive = 1
var arr1 = [1, 2, 3, 4];
var arr2 = ["1", "g", "s"];
// tuble 
var tuple = ["@", 2];
// enmun: không thể thây đỏi giá trị 
var Color;
(function (Color) {
    Color["black"] = "#000";
    Color["white"] = "#fff";
    Color["red"] = "#f00";
})(Color || (Color = {}));
console.log(Color.black);
var n = null;
var u = undefined;
var a = 4;
var result = null;
var notSure = 5;
notSure = "Sds";
var sinhVien = {
    hoten: "manh",
    lop: "FE50",
};
console.log(sinhVien);
// Optional chaining 
// data = {}
// data.thongTinPhim = undefined;
// data.thongTinPhim.lichChieu = > lỗi
// data?.thongTinPhim?.lichChieu?.map()
// ? kiểm tra vế đàng trước có undefined hay k
// function
function fool() {
    console.log("hoisting");
}
; //hoisting thân hàm và thân hàm => có thể goi đước trước khi tạo cái hàm đó ra
var foo2 = function () { }; //chỉ hoisting tên hàm => k thể goi hàm trước khi tạo hàm
var foo3 = function (hoTen, lop) { return hoTen + " - " + lop; }; // kiểu dữ liệu trả về cho hàm
// void: sử dung cho hàm k return
var foo4 = function () {
    console.log("fooo4");
};
var foo5 = function (value, total) {
    if (value === void 0) { value = 5; }
    if (total === void 0) { total = value * 0.1; }
    console.log(total);
};
var displayColor = function () {
    var color = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        color[_i] = arguments[_i];
    }
    for (var i_2 = 0; i_2 < color.length; i_2++) {
        console.log(color[i_2]);
    }
};
displayColor('pink', 'red');
// <Detail hoTen={sinhVien.hoten} lop={sinhVien.lop} tuoi ={sinhVien.tuoi}/>
// <Detail {...sinhVien}/>
// Destructuring
var date = ['05', '08', '2020'];
// const day = date[0];
// const mon = date[1];
// const year = date[2];
var day = date[0], mon = date[1], year = date[2];
var day1 = date[0], year1 = date[2];
var hoten = sinhVien.hoten, tuoi = sinhVien.tuoi, lop = sinhVien.lop;
var fullname = sinhVien.hoten, age = sinhVien.tuoi, className = sinhVien.lop;
// Class
var NhanVien = /** @class */ (function () {
    function NhanVien(hoTen) {
        this.hoTen;
    }
    NhanVien.prototype.tinhLuong = function () {
        return 1000;
    };
    return NhanVien;
}());
var GiamDoc = /** @class */ (function (_super) {
    __extends(GiamDoc, _super);
    function GiamDoc(hoten, heSoLuong) {
        var _this = _super.call(this, hoten) || this;
        _this.heSoLuong = heSoLuong;
        return _this;
    }
    // Override lại hamg tính lương của nhân viên : ghi đè
    GiamDoc.prototype.tinhLuong = function () {
        return _super.prototype.tinhLuong.call(this) * this.heSoLuong;
    };
    return GiamDoc;
}(NhanVien));
var NhanVienThietKe = /** @class */ (function () {
    function NhanVienThietKe() {
    }
    // tuoi:number;
    NhanVienThietKe.prototype.thucHienCongViec = function () {
        console.log("thiết kế nội thức");
    };
    NhanVienThietKe.prototype.tinhLuong = function () {
        return 5000;
    };
    return NhanVienThietKe;
}());
// DOM TS
var txtEmail = document.getElementById("txtEmail").value;
console.log(txtEmail);
