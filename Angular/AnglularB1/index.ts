console.log('Hello Typescript');
// var vs let
// scope : var  = function scope, let block scope
// hoisting:
// console.log(a);
// var a = 5;

// console.log(b);
// let b = 5

for(var i = 0 ; i < 5;i++){
    setTimeout(() => {
        console.log(i);
        
    }, 1000);
}



for(let i = 0 ; i < 5;i++){
    setTimeout(() => {
        console.log(i);
        
    }, 1000);
}

// IIFE function :là function đc khơi tạo và chạy ngay khi khởi tạo

// function loop(i)
for(var i = 0 ; i < 5;i++){
  (function(i){
    setTimeout(() => {
        console.log(i);
        
    }, 1000);
  })(i);
}

// khai báo kiểu dữ liệu

let messages:string = 'hello';
// messages = 5
let diemToan:number = 10;
// diemToan = "sda"
let isActive:boolean = true;
// isActive = 1
let arr1:Array<number> = [1,2,3,4];
let arr2:string[]= ["1","g","s"];

// tuble 
let tuple:[string,number]=["@",2];

// enmun: không thể thây đỏi giá trị 
enum Color {black = "#000",white ="#fff",red="#f00"}
console.log(Color.black);



let n:null =null;
let u:undefined = undefined;
let a:string | number = 4;
let result:string | null = null
let notSure:any = 5;
notSure = "Sds";

type SinhVien ={
    hoten:string;
    lop:string;
    tuoi?:number //? có thể cso or k
}
const sinhVien:SinhVien ={
    hoten:"manh",
    lop:"FE50",
    // tuoi:21
}
console.log(sinhVien);
// Optional chaining 
// data = {}
// data.thongTinPhim = undefined;
// data.thongTinPhim.lichChieu = > lỗi
// data?.thongTinPhim?.lichChieu?.map()
// ? kiểm tra vế đàng trước có undefined hay k


// function
function fool(){
    console.log("hoisting");
    
};//hoisting thân hàm và thân hàm => có thể goi đước trước khi tạo cái hàm đó ra
const foo2 =function(){};//chỉ hoisting tên hàm => k thể goi hàm trước khi tạo hàm
const foo3 =(hoTen:string,lop:string):string =>{  return `${hoTen} - ${lop}`   };// kiểu dữ liệu trả về cho hàm
// void: sử dung cho hàm k return
const foo4 =():void=>{
    console.log("fooo4");
    
}
const foo5 =(value = 5,total = value*0.1)=>{
    console.log(total);
    
};

const displayColor =(...color:string[])=>{
    for(let i = 0;i < color.length;i++){
        console.log(color[i]);
        
    }
}
displayColor('pink','red');

// <Detail hoTen={sinhVien.hoten} lop={sinhVien.lop} tuoi ={sinhVien.tuoi}/>
// <Detail {...sinhVien}/>

// Destructuring
const date = ['05','08','2020'];
// const day = date[0];
// const mon = date[1];
// const year = date[2];
const [day,mon,year] = date;
const [day1,,year1]= date;

const {hoten,tuoi,lop} = sinhVien;
const {hoten:fullname,tuoi:age,lop:className} = sinhVien;

// Class

class NhanVien {
    // private : Chỉ sử dụng đc bên trong class
    // protected:private + vc sữ dung đc bên trong class kế thừa
    //public:sử dụng đươc ở bên ngoài
    protected hoTen:string;

    constructor(hoTen:string) {
        this.hoTen
    }
    tinhLuong():number{
        return 1000
    }
}

class  GiamDoc extends NhanVien {
    private heSoLuong:number;
    constructor(hoten:string,heSoLuong:number) {
        super(hoten)
        this.heSoLuong = heSoLuong
    }

    // Override lại hamg tính lương của nhân viên : ghi đè
    tinhLuong():number{
        return super.tinhLuong() * this.heSoLuong
    }
}

// interface
// k có contructor ||chỉ khai báo tên hmaf thôi k viếc j trong cả

interface Demo{
    test:string
}

interface INhanVien{
    hoTen:string;
    tuoi?:number;
    thucHienCongViec?:() => void;
    tinhLuong():number;
}
class  NhanVienThietKe implements INhanVien,Demo {
    // public hết
    test:string;
    hoTen:string;
    // tuoi:number;
    
    thucHienCongViec():void{
        console.log("thiết kế nội thức");
        
    }
    tinhLuong():number{
        return 5000
    }
}
// DOM TS
const txtEmail = (<HTMLInputElement>document.getElementById("txtEmail")).value
console.log(txtEmail);

