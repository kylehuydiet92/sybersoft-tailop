// call api lây danh sách câu hỏi
let questionList = [];


const fetchQuestion = function(){
    axios({
        url:'https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/questions',
        method:'GET',
        data: null
    }).then(function(res){
        console.log(res);
        mapData(res.data);
        renderQuestion();
    }).catch(function(err){
        console.log(err);
    })
}
fetchQuestion();

const renderQuestion = function(){
    let htmlContent = "";
    for(let index in questionList){
     htmlContent += questionList[index].render(+index + 1);
    }
    htmlContent +=`
        <button class="btn btn-info my-3" onclick="xemKetQua()">Xem Ket Qua</button>
    `
    document.getElementById("content").innerHTML = htmlContent; 
}

const xemKetQua = function(){
        let result = 0;
        questionList.forEach(function(question,index){
            if(question.checkExact()) result++;
        })
        alert("Kết quả số câu đúng là : " + result);
}

const mapData = function(data){
    for(let question of data){
        let newQuestion;
        if(question.questionType === 1){
            newQuestion = new MultipleChoice(question.questionType,question.id,question.content,question.answers);
        }else{
            newQuestion = new FilllnBlank(question.questionType,question.id,question.content,question.answers);
        }

        questionList.push(newQuestion)
    }
    console.log(questionList);
}



//3 trang thức của promise
//pending
//fulfill
//reject