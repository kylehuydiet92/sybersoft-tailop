class FilllnBlank extends Question{
    constructor(type,id,content,answers){
        super(type,id,content,answers)
    }

    checkExact(){
        const traloi = document.getElementById(`traLoi-${this.id}`).value;
        if(traloi === this.answers[0].content){
            return true;
        }return false;

    }

    // render Html
    render(index){
        return `
            <div>
                <h3>Câu hỏi ${index}: ${this.content}</h3>
                <input type="text" id="traLoi-${this.id}">
            </div>
        `
    }
}

const newFilllnBlank = new FilllnBlank(1,'10',"Hôm nay là thứ mấy",[
    {content:"thứ 2"},
    {content:"thứ 3"},
    {content:"thứ 4"},
    {content:"thứ 5"},
    {content:"thứ 6"},
]);

// console.log(newFilllnBlank.render());