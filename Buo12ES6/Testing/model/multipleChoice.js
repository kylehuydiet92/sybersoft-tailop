class MultipleChoice extends Question {
  constructor(type, id, content, answers) {
    // super ke thua laij tunog tinh cuar thang cha
    super(type, id, content, answers);
  }

  /**
   * cau tra loi co dung hay khong ?
   * neu dung tra ve true
   * new sai tra ve false
   *
   *  Các bước thực hiện
   * 1. Lấy kết quả đã chọn
   * 2. kiểm tra xem kết quả chọn đúng hay khhong
   *  neu dung tra ve true
   * new sai tra ve false
   */
  checkExact() {
    
    const inputList = document.getElementsByClassName(`manh-${this.id}`);
    // lấy kết quả đã chọn
    let answerId;
    
    for(const input of inputList){
       //kiểm tra xem có checked k
       if (input.checked) {
        answerId = input.value;

      }
    }

    let ketqua = false;
    //2. kiểm tra xem kết quả chọn đúng hay khhong
    this.answers.forEach(function(item,index){
      //lấy ra câu trả lời dúng
      if(item.exact){
        // ktra co dung vs cau tra loi dung vs cau chon không
        if(item.id === answerId){
          ketqua =  item.exact ;   
        }
      
      }
    })
    return ketqua;
  

}

  

  // output : đoạn html của MultipleChoice
  render(index) {
    let answersHtml = "";
    // for(let i = 0 ; i < this.answers.length;i++){
    //     console.log(this.answers[i])
    //     answersHtml +=`
    //         <div>
    //             <input type="radio"/>
    //             <label>${this.answers[i].content}</label>
    //         </div>
    //     `
    // }

    for (let ans of this.answers) {
      answersHtml += `
              
                    <label class="d-block">
                        <input class="manh-${this.id}" value="${ans.id}" type="radio" name = "manh-${this.id}"/>
                        ${ans.content}
                    </label>
               
            `;
    }

    return `
            <div>
                <h3>Câu hỏi ${index}: ${this.content}</h3>
               <div>${answersHtml}</div>
            </div>
        `;
  }
}

const newMultipleChoice = new MultipleChoice(1, "10", "Hôm nay là thứ mấy", [
  { content: "thứ 2" },
  { content: "thứ 3" },
  { content: "thứ 4" },
  { content: "thứ 5" },
  { content: "thứ 6" },
]);
// console.log(newMultipleChoice.render());
