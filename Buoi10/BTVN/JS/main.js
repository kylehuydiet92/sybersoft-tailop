var completeTasks = [];
var completeTaskCheck = [];
var idUpdate = null;

function addTask() {
  //lấy value nhâp vào input
  const contentTack = document.getElementById("txtTask").value;
  var idTask = Math.random();
  var isValid = true;
  isValid &=
    checkTask(contentTack, "txtTask") &&
    lengthCheck(contentTack, "txtTask", 3, 30);

  //khơi tao đoi tương task
  if (isValid) {
    const newTask = new Task(idTask, contentTack);
    completeTasks.push(newTask);
    //console.log(completeTasks);

    renderTask();
    saveData();
   

  }
}

function renderTask(data) {
  htmlContent = "";

  data = data || completeTasks;
  for (var i = 0; i < data.length; i++) {
    const curentTask = data[i];
    htmlContent += `<div class="tack__content__card__item__add">
        <div class="tack__content__card__item__add__item"> <span>${curentTask.contentTack}</span></div>
        
        <div class="tack__content__card__item__add__item1">
          <i class="fa fa-pen dont__check"   onClick="getTask('${curentTask.idTask}')"></i>
          <i class="fa fa-trash-alt dont__check"   onClick="deleteTask('${curentTask.idTask}')"></i>
          <i class="fa fa-check-circle" onClick="checkWorkTask('${curentTask.idTask}')"></i>
        </div>
      </div>`;
  }
  //console.log(htmlContent);
  document.getElementById("tackcontentadd").innerHTML = htmlContent;
  const contentTack = (document.getElementById("txtTask").value = "");
}

//save Data
function saveData() {
  // console.log(JSON.stringify(completeTasks));
  localStorage.setItem("listTask", JSON.stringify(completeTasks));
 
}
function saveDataChek(){
  localStorage.setItem("listCheckTask", JSON.stringify(completeTaskCheck));

}

// function saveData(key,value){
//   localStorage.setItem(key, JSON.stringify(value));
// }
// lay data dưới localStorage in ra màn hình
function fetchData() {
  const localListData = localStorage.getItem("listTask");

  if (localListData) {
    // console.log(completeTasks);
    completeTasks = JSON.parse(localListData);
    
    //   console.log(completeTasks);
    renderTask();
  }
}

function fetchDataCheck() {
  const localListDataCheck = localStorage.getItem("listCheckTask");

  if (localListDataCheck) {
    console.log("befo",completeTaskCheck);
    completeTaskCheck = JSON.parse(localListDataCheck);
      console.log("after",completeTaskCheck);
    renderTaskCheck(completeTaskCheck);
  }
}

// function fetchData(value,key,list) {
//   const value = localStorage.getItem(key);

//   if (value) {
//     // console.log(completeTasks);
//     list = JSON.parse(value);
    
//     //   console.log(completeTasks);
//     renderTask();
//   }
// }

/**-------------------------------------------------------- */



//update Task
function getTask(id) {
  const index = findById(id);

  if (index !== -1) {
    //dưa thông tin lên input
    document.getElementById("txtTask").value = completeTasks[index].contentTack;

    //lưu lại id task cần sửa
    idUpdate = completeTasks[index].idTask;
  }

  document.getElementById("add").style.display = "none";
  document.getElementById("update").style.display = "block";
}



function updateTask() {
  const contentTack = document.getElementById("txtTask").value;

  const index = findById(idUpdate);

  const updateTasks = new Task(idUpdate, contentTack);

  completeTasks[index] = updateTasks;
  //console.log(updateTasks);
  // saveData("listTask",completeTasks);
  saveData();

  document.getElementById("txtTask").value = "";
  idUpdate = null;
 // console.log("ok");
  renderTask();
  //khi update xong thì trả về mặc 0ịnh
  document.getElementById("add").style.display = "block";

  document.getElementById("update").style.display = "none";
  //document.getElementById("updatecheck").style.color = " #9b9b7a";
}
//valiable
function checkTask(value, idError) {
  if (value) {
    return true;
  } else {
    alert("*Vui lòng nhập vào ô input !");
  }
}

function lengthCheck(value, idError, minlength, maxlength) {
  if (value.length < minlength || value.length > maxlength) {
    alert(`*Đọ dài phải từ ${minlength} đến ${maxlength}`);

    return false;
  } else {
    return true;
  }
}

//hàm check Task
function checkWorkTask(id) {
  const index = findById(id);
  //console.log(findById(id));
  if (index !== -1) {
    idCheck = completeTasks[index].idTask;
    contetCheck = completeTasks[index].contentTack;
    //console.log(idCheck, contetCheck);

    const newCheckTask = new CheckTask(idCheck, contetCheck);
   // console.log(newCheckTask);
    completeTaskCheck.push(newCheckTask);
    //console.log(completeTaskCheck);
    completeTasks.splice(index, 1);

    saveData();
 
    renderTask();
    saveDataChek();
    // saveData("listCheckTask",completeTaskCheck);
  
    renderTaskCheck(completeTaskCheck);
  
  } else {
    alert("Opps !!!! Something went wrong !");
  }
}

function renderTaskCheck(data) {
  htmlContentCheck = " ";

 

  for (var i = 0; i < data.length; i++) {
    const curentTask = data[i];
    htmlContentCheck += `
                          <div class="tack__content__card__item__add__check">
                            <div class="tack__content__card__item__add__item">
                              <span>${curentTask.TaskCheck}</span>
                            </div>
                            <div class="tack__content__card__item__add__item1">
                                
                                  <i class="fa fa-trash-alt dont__check" onClick="deleteTaskCheck('${curentTask.idChek}')"></i>
                                  <i class="fa fa-check-circle"></i>
                           </div> 
                          </div>
                          `;
  }
 // console.log(htmlContentCheck);
  document.getElementById("taskcheck").innerHTML = htmlContentCheck;
  // const contentTack = document.getElementById("txtTask").value = "";
}

//reseset input
function reset() {
  document.getElementById("add").style.display = "block";

  document.getElementById("update").style.display = "none";
  document.getElementById("txtTask").value = "";
}

//lay id taskcheck

function findByIdCheck(id) {
  for (var i = 0; i < completeTaskCheck.length; i++) {
    if (completeTaskCheck[i].idChek === parseFloat(id)) {
      return i;
    }
  }
  return -1;
}

function findById(id) {
  for (var i = 0; i < completeTasks.length; i++) {
    if (completeTasks[i].idTask === parseFloat(id)) {
      return i;
    }
  }
  return -1;
}



//xóa task
function deleteTask(id) {
  const index = findById(id);
  //console.log(findById(id));
  if (index !== -1) {
    completeTasks.splice(index, 1);
    // saveData("listTask",completeTasks);
    saveData();
    renderTask();
  } else {
    alert("Opps !!!! Something went wrong !");
  }
}

function deleteTaskCheck(id) {
  const index = findByIdCheck(id);
  //console.log(findById(id));
  if (index !== -1) {
    completeTaskCheck.splice(index, 1);
    saveDataChek();
    // saveData("listCheckTask",completeTaskCheck);
    renderTaskCheck(completeTaskCheck);
  } else {
    alert("Opps !!!! Something went wrong !");
  }
}



fetchData();
fetchDataCheck();