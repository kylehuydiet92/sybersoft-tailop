//2 câu phỏng vấn
//1. cho môt mảng số nguyên kiểm tra 2 số cí bất kì tổng là 10 k  có trả về là true
// function checkSum(arr){
//     for(var i = 0; i < arr.length - 1;i++){
//         for(var j = i+1;j< arr.length;j++){
//             if(arr[i] + arr[j] === 10) return true;
//         }
//     }
//     return false;
// }
// console.log( checkSum([4,2,3,1,6,5,9])); 

// O(n^2);



//2 cho một mạng số nguyên sắp xếp sẳn 
//cho môt mảng số nguyên kiểm tra 2 số cí bất kì tổng là 10 k >
function checkSum(arr){
    // Thuât toán multiple point
    var i = 0 ;
    var j = arr.length - 1;

    while(i < j){
        if(arr[i] + arr[j] ===10)
            return true;
        if(arr[i]+arr[j] > 10) j--;
        else i++
    }
    return false;
}

console.log(checkSum([1,2,3,4,5,6,7,8,9]));

//Big0Notation
