/**
 * Dự án quản lý nhân viên
 * Người tạo :mạnh
 * Chức năng:
 * 	1/ Cho phép nhân sự thêm nhân viên mới 
 * 	2/ Hiểm thị danh sách tất cả nhân viên trong công ty
 * 	3/ Xóa nhân viên
 * 	4/ cập nhâp thông tin của nhân viên
 * 	5/ Tìm kiếm nhân viên
 * 	6/ Găn valudation form (kiểm tra dữ liệu vào)
 *	7/Sắp xếp nhân viên theo theo tên 
	8/In ra nhân viên có lương cao nhất
	9/
 */

//3 phân rả lớp đối tượng
/**
 * NV:lastName,firstName,emolId,startedDate,postion
 * calcSalary()
 */
//tao doi mạng chứa nhân viên
var employeeList = [];

//b1 function 1 : them nhân viên
function addEmployee() {
  /**
   * 1. Dom tới inputs lay values
   * 2. tao ra một đối tượng nhan vien chưa hết tất cả thông tin của nhân viên
   * push vào employeeList
   */
  const lastName = getEl("ho").value;
  const firstName = getEl("ten").value;
  const employeeId = getEl("msnv").value;
  const startedDate = getEl("datepicker").value;
  const postion = getEl("chucvu").value;

  // if(checkRequired(lastName,"lastNameError")){
  //   checkLength(lastName,"lastNameError",3,10);
  // }

  //validiation cái form

  // var a&=b a= a&b
  var isValid = true;

  isValid &= lastNameValidation= checkRequired(lastName, "lastNameError") &&
    checkLength(lastName, "lastNameError", 3, 10) &&
    checkString(lastName, "lastNameError");

  // isValid & = isvalid & false =>> false 

  isValid &= firstNameValidation= checkRequired(firstName, "firstNameError") &&
    checkLength(firstName, "firstNameError", 3, 10) &&
    checkString(firstName, "firstNameError");
  //checkRequired(employeeId, "idError");

  if(isValid ){
//check độ dài length

  const index = findByID(employeeId);
  //kiểm tra id tồn tại
  if (index !== -1) {
    alert("mã đã tồn tại");
    return;
  }

  // [nv1,nv2,nv3]
  //kiemtra tôn tại

  // uu tiên sai const new không dôi j hết
  const newEmployee = new Employee(
    lastName,
    firstName,
    employeeId,
    startedDate,
    postion
  );

  employeeList.push(newEmployee);

  //in ds nhan vien
  renderEmloyee();
  //luu danh sach nhan vien va local lần sau lấy lên sai tiếp
  saveData();


  }

  

  // console.log(employeeList);
}

// hiển thị nhân viên ra màn hình

function renderEmloyee(data) {
  var htmlContent = "";

  // if(!data){
  //    data = employeeList;
  // }

  //data = undefined

  //fasethy 0 null und false ''
  data = data || employeeList;

  for (var i = 0; i < data.length; i++) {
    const curentEmpl = data[i];
    htmlContent += `
		<tr>
			<td>${i + 1}</td>
			<td>${curentEmpl.lastName}  ${curentEmpl.firstName}</td>
			<td>${curentEmpl.emplId}</td>
			<td>${curentEmpl.startedDate}</td>
			<td>${curentEmpl.postiom}</td>
      <td>${curentEmpl.calcSalary()}</td>
      <td>
        <button class=" btn btn-danger rounded-circle" 
          onClick="deleteEmpl('${curentEmpl.emplId}')">
          <i class="fa fa-trash">
         </i></button>
         <button class=" btn btn-info rounded-circle" 
          onClick="getEmpl('${curentEmpl.emplId}')">
          <i class="fa fa-pencil">
         </i></button>

      </td>
		</tr>
		`;
  }
  //console.log(htmlContent);
  document.getElementById("tbodyEmployeeList").innerHTML = htmlContent;
}

//function 3: lưu giữ liệu vào localStorage
function saveData() {
  // chuyển employeeList sang ==> chuổi JSON
  console.log(JSON.stringify(employeeList));
  localStorage.setItem("employeeList", JSON.stringify(employeeList));
}

//function4 lay du lieu tu local va in ra man hinh ngay khi load
function fetchData() {
  const localEmplList = localStorage.getItem("employeeList");
  //kiem tra ton tai
  if (localEmplList) {
    //chuyển lại thành chuổi object
    // employeeList = JSON.parse(localEmplList);
    console.log("before", employeeList);
    mapData(JSON.parse(localEmplList));
    //in danh sach hien tai ra màn hình
    console.log("after", employeeList);
    renderEmloyee();
  }
}

//function 5: xóa nhan viên
function deleteEmpl(id) {
  //splice(start,1)
  //input : id  của nhân viên muốn xóa
  /**
   * process:
   * //1 tìm vi tri của id findbyid
   * //2 splice()
   * //3 render lại giao diện
   */
  // tìm vi trí
  const index = findByID(id);
  //kiem tra loi vs splice()
  if (index !== -1) {
    employeeList.splice(index, 1);
    //cần xóa data dưới local
    //saveData();
    renderEmloyee();
  } else {
    alert("Opps !!!! Something went wrong !");
  }
}

//function6: tìm kiếm theo tên or mã
//tim theo ma
function searchEmpl() {
  var result = [];
  var keyword = getEl("txtSearch").value;

  //lay từng thang trong ds nhân viên thằng  nweews có thì push nó vào result
  //duyệt ds nvien,ktra từng nv1 ,nv nào đó có id giống với ketwword
  for (var i = 0; i < employeeList.length; i++) {
    if (employeeList[i].emplId === keyword) {
      //result = [];
      result.push(employeeList[i]);
      break;
    }
    var fullname = employeeList[i].lastName + " " + employeeList[i].firstName;

    //convert ketword

    fullname = nonAccentVietnamese(fullname);
    keyword = nonAccentVietnamese(keyword).trim();

    //chuyển vê chử thường
    if (fullname.indexOf(keyword) !== -1) {
      result.push(employeeList[i]);
    }
  }
  //input : keyword
  renderEmloyee(result);

  //expectation: result
}

// function 7: Phana1 lấy dữ liêu của nhân viên muốn sữ , đưa lên form
function getEmpl(id) {
  //input id nhân viên muốn sữa
  const index = findByID(id);
  if (index !== -1) {
    const updateEmpl = employeeList[index];

    //2 dua thong tin len form
    getEl("ho").value = updateEmpl.lastName;
    getEl("ten").value = updateEmpl.firstName;
    getEl("msnv").value = updateEmpl.emplId;
    getEl("datepicker").value = updateEmpl.startedDate;
    getEl("chucvu").value = updateEmpl.postiom;

    //khoa ô mã số nv lại
    getEl("msnv").setAttribute("disabled", true);

    // 4. ân đi nút thêm,hiên nút sữa
    const themnhanvien = getEl("themnhanvien");
    themnhanvien.style.display = "none";
    const luuthaydoi = getEl("luuthaydoi");
    luuthaydoi.style.display = "block";
  }

  //prcess:
  /**
   * 1. duyết danh sách nhân viên, tìm ra nhân viên muôn sữ nằm ở vitri nào
   * 2. đưa thông tin lên form
   */
}

//function8: Phần 2 chỉnh sữa  thông tin nhân viên
function updateEmpl() {
  //1. Lấy lại thông tin trên form
  //2. bốc nó ra cập nhap lại dựa vào id tìm vị trí trong mảng ==> cập nhập từng thuocs tính một lại cho nó
  //vi tri : index ==> emplist[index].lastName = dom
  const lastName = getEl("ho").value;
  const firstName = getEl("ten").value;
  const employeeId = getEl("msnv").value;
  const startedDate = getEl("datepicker").value;
  const postion = getEl("chucvu").value;

  const index = findByID(employeeId);

  const updateEmployee = new Employee(
    lastName,
    firstName,
    employeeId,
    startedDate,
    postion
  );

  employeeList[index] = updateEmployee;
  saveData();
  renderEmloyee();

  const themnhanvien = getEl("themnhanvien");
  themnhanvien.style.display = "block";
  const luuthaydoi = getEl("luuthaydoi");
  luuthaydoi.style.display = "none";
  getEl("msnv").removeAttribute("disabled");
  // clear form
  getEl("btnReset").click();
}

//convert vn sang en
function nonAccentVietnamese(str) {
  str = str.toLowerCase();
  //     We can also use this instead of from line 11 to line 17
  //     str = str.replace(/\u00E0|\u00E1|\u1EA1|\u1EA3|\u00E3|\u00E2|\u1EA7|\u1EA5|\u1EAD|\u1EA9|\u1EAB|\u0103|\u1EB1|\u1EAF|\u1EB7|\u1EB3|\u1EB5/g, "a");
  //     str = str.replace(/\u00E8|\u00E9|\u1EB9|\u1EBB|\u1EBD|\u00EA|\u1EC1|\u1EBF|\u1EC7|\u1EC3|\u1EC5/g, "e");
  //     str = str.replace(/\u00EC|\u00ED|\u1ECB|\u1EC9|\u0129/g, "i");
  //     str = str.replace(/\u00F2|\u00F3|\u1ECD|\u1ECF|\u00F5|\u00F4|\u1ED3|\u1ED1|\u1ED9|\u1ED5|\u1ED7|\u01A1|\u1EDD|\u1EDB|\u1EE3|\u1EDF|\u1EE1/g, "o");
  //     str = str.replace(/\u00F9|\u00FA|\u1EE5|\u1EE7|\u0169|\u01B0|\u1EEB|\u1EE9|\u1EF1|\u1EED|\u1EEF/g, "u");
  //     str = str.replace(/\u1EF3|\u00FD|\u1EF5|\u1EF7|\u1EF9/g, "y");
  //     str = str.replace(/\u0111/g, "d");
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  // Some system encode vietnamese combining accent as individual utf-8 characters
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng
  str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư
  return str;
}

//tim theo ten

//function :Chuyeexn đổi dữ liệu từ localStorage thành giữ liêu mới
//chỉ để lấy phương thức
function mapData(data) {
  for (var i = 0; i < data.length; i++) {
    //Đối tượng nhân viên từ local :data[i]
    // ==> chuyễn thành đối tượng nhân viên mới: newEmpl
    const newEmpl = new Employee(
      data[i].lastName,
      data[i].firstName,
      data[i].emplId,
      data[i].startedDate,
      data[i].postiom 
    );
    employeeList.push(newEmpl);
  }
}

function getEl(id) {
  return document.getElementById(id);
}

function findByID(id) {
  for (var i = 0; i < employeeList.length; i++) {
    if (employeeList[i].emplId === id) {
      return i;
    }
  }
  return -1;
}

//fetch dữ liêu từ localstaorage len từ ngay lúc đầu
fetchData();

//----------------------------------------VALIDATION

//kiểm tra validation
//các loai validation chính : required : bắt buột nhập
// length: kieemtr tran độ dài maxlength,minlength
//Pttern

function checkRequired(value, idError) {
  if (value) {
    getEl(idError).innerHTML = "";
    return true;
  } else {
    getEl(idError).innerHTML = "*Trường này bắt buộc nhập !";
    return false;
  }
}

// kiêm tra thi phải tra về true or false
//kieemt tra length check length
function checkLength(value, idError, minLength, maxlength) {
  if (value.length < minLength || value.length > maxlength) {
    getEl(idError).innerHTML = `Độ dài phải từ  ${minLength} đến ${maxlength}`;
    return false;
  } else {
    getEl(idError).innerHTML = ``;
    return true;
  }
}

//kiểm tra patten
function checkString(value, errorId) {
  const pattern = new RegExp(
    "^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêếìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶ" +
      "ẸẺẼỀẾỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợ" +
      "ụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$"
  );

  if (pattern.test(value)) {
    getEl(errorId).innerHTML = "";
    return true;
  } else {
    getEl(errorId).innerHTML = "*Dữ liêu không đúng định dạng !";
    return false;
  }
}
