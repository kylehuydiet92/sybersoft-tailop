/**
 * Người tạo : mạnh
 * Chức năng app : quản lý khóa học
 * Chức năng:
 *      Hiên danh sách khóa học
 *      Xem chi tiết kháo học
 *
 */

//expression function
/**
 * 
 * fetchCorses();
    function fetchCorses(){
    console.log("A");
    }

 */
//declaration funciton
var corurseList =[];
const fetchCorses = function () {
   axios({
    url:
      "http://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01",
    method: "GET",
    //dể gửi cái j đo cho backend
    data: null,
  })
  //callback Function
  //truyền môt cái hàm(có tham so) vào trong một hàm  được gọi là callback function

    .then(function (res) {
      console.log("response form backend", res);
      corurseList = res.data;
      rederCourses();
  
    })
    .catch(function (err) {
      console.log(err);
    });
    console.log(corurseList);
  /**
   * Promise (pending,)
   */
};

fetchCorses();

//vidu

const rederCourses = function(){
    var htmlContent = "";
    for( var i = 0;i < corurseList.length;i++){
        htmlContent +=`
        <div class="col-4">
        <div class="card mb-4 p-3">
          <img src="${corurseList[i].hinhAnh}" alt=""  style="height:250px"/>
          <h4>${corurseList[i].tenKhoaHoc}</h4>
          <p>${corurseList[i].nguoiTao.hoTen}</p>
          <p>
              <span class="text-warning"><i class="fa fa-star"></i></span>
              <span class="text-warning"><i class="fa fa-star"></i></span>
              <span class="text-warning"><i class="fa fa-star"></i></span>
              <span class="text-warning"><i class="fa fa-star"></i></span>
              <span class="text-warning"><i class="fa fa-star"></i></span>
          </p>
          <button class = "btn btn-success" onclick = "viewDetail('${corurseList[i].maKhoaHoc}')">Xem Chi Tiết</button>
        
        </div>
        
      </div>
        `;
    }
    document.getElementById("courseSection").innerHTML = htmlContent;
}

const viewDetail = function(id){
    window.location.assign(`detail.html?id=${id}`);
}

