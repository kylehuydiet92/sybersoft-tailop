var ProductList = [];
var productService = new ProductService();
var idProduct = -1;
// Crud load
const fetchProductCrud = function () {
  productService
    .getListProduct()
    .then(function (res) {
      mapData(res.data);

     
    
      renderCurdTable( );
   
    })
    .catch(function (err) {
      console.log(err);
    });
  // console.log(ProductList);
};

// render table CUrd
const renderCurdTable = function (data) {
  data = data  || ProductList;
 
  htmlContents = "";
  for (var i = 0; i < ProductList.length; i++) {
    htmlContents += `
            <tr>
            <td>  ${i + 1}</td>
            <td>  ${data[i].name}</td>
            <td>${data[i].description}</td>
            <td>${data[i].price}</td>
            <td>${data[i].type}</td>
            <td>${data[i].inventory}</td>
            <td><img class="img-fluid" src="${
              data[i].image
            }" alt="" style="height: 30px; width: 30px;"></td>
            <td>
                <button class="btn btn-danger" onclick="DeleteProduct( '${
                  data[i].id
                }')"><i class='bx bx-trash' ></i></button>
                <button class="btn btn-primary" onclick="getIfromationProduct( ${
                  data[i].id
                })"><i class='bx bxs-pencil' ></i></button>
            </td>
        </tr>
            `;
    getEl("tbodyCrud").innerHTML = htmlContents;
  }
  //   console.log("table", htmlContents);
};

// Theem nhan vien
const AddProduct = function () {
  const nameProduct = getEl("nameProduct").value;
  const description = getEl("description").value;
  const price = getEl("price").value;
  const type = getEl("type").value;
  const inventory = getEl("inventory").value;
  const image = getEl("imgLink").value;
  var newProduct = new Product(
    nameProduct,
    description,
    price,
    type,
    inventory,
    image
  );

  var isValid = true;
  isValid &=
    checkRequired(nameProduct, "idName", "*Vui long nhap thong tin vao !") &&
    checkLength(nameProduct, "idName", 3, 20);

  isValid &=
    checkRequired(
      description,
      "errDescription",
      "*Vui long nhap thong tin vao !"
    ) && checkLength(description, "errDescription", 3, 50);

  isValid &=
    checkRequired(price, "errPrice", "*Vui long nhap thong tin vao !") &&
    checkLength(price, "errPrice", 1, 10) &&
    checkNumber(price, "errPrice");

  isValid &= checkRequired(type, "errType", "*Vui long nhap thong tin vao !");

  isValid &=
    checkRequired(
      inventory,
      "errInventory",
      "*Vui long nhap thong tin vao !"
    ) &&
    checkLength(inventory, "errInventory", 1, 4) &&
    checkNumber(inventory, "errInventory");

  isValid &=
    checkRequired(image, "errLink", "*Vui long nhap thong tin vao !") &&
    checkURL(image, "errLink");

  if (isValid) {
    productService
      .addProduct(newProduct)
      .then(function (res) {
        alert("Thêm thành công !");

        fetchProductCrud();
        resetValue();

        //   console.log(newProduct);
      })
      .catch(function (err) {
        console.log(err);
      });
    // console.log("hello");
  }
};
//Xóa product
const DeleteProduct = function (id) {
  console.log(id);
  productService
    .deleteProduct(id)
    .then(function (res) {
      alert("xoa nguoi dung thanh cong !");

      fetchProductCrud();
    })
    .catch(function (err) {
      console.log(err);
    });
};

// -----------------------------------------------

// get product information
const getIfromationProduct = function (id) {
  // console.log(idProduct);
  getEl("txtCurd").innerHTML = "Update Product";
  getEl("addproduct").style.display = "none";
  getEl("updateproduct").style.display = "block";
  getEl("cancelUpdate").style.display = "block";
  axios({
    url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
    method: "GET",
    data: null,
  })
    .then(function (res) {
      getEl("nameProduct").value = res.data.name;
      getEl("description").value = res.data.description;
      getEl("price").value = res.data.price;
      getEl("type").value = res.data.type;
      getEl("inventory").value = res.data.inventory;
      getEl("idProduct").value = res.data.id;
      getEl("imgLink").value = res.data.image;
    })
    .catch(function (err) {
      console.log(err);
    });
};
//Sort Product
//sort tang daan
const sortAscendingPrice = function (data) {
  var temp;
  for (var i = data.length; i > 0; i--) {
    for (var j = 0; j < i - 1; j++) {
    
      if (+data[j].price > +data[j + 1].price) {
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }
  return data;
};

// so sánh kí tự
const sortAscendingName = function (data) {
  var temp;
  for (var i = data.length; i > 0; i--) {
    for (var j = 0; j < i - 1; j++) {
    
      if (data[j].name > data[j + 1].name) {
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }
  return data;
};

//sort giam dan
const sortDecreasePrice = function (data) {
  var temp;
  for (var i = data.length; i > 0; i--) {
    for (var j = 0; j < i - 1; j++) {

      if (+data[j].price < +data[j + 1].price) {
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }
  return data;
};
// so sánh kí tự
const sortDecreaseName = function (data) {
  var temp;
  for (var i = data.length; i > 0; i--) {
    for (var j = 0; j < i - 1; j++) {
      console.log(j);
      if (data[j].name < data[j + 1].name) {
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }
  return data;
};
const sortSelect = function(){
  
  var sort = document.getElementById("mySelect").value;
  if(sort == 1){
     return renderCurdTable(sortAscendingPrice(ProductList))
 
  }if(sort == 2){
    return renderCurdTable( sortDecreasePrice(ProductList));
   
  }
  if(sort == 3){
    return renderCurdTable( sortAscendingName(ProductList));
   
  }if(sort == 4){
    return renderCurdTable( sortDecreaseName(ProductList));
   
  }
 
}






// Update Product
const updateProduct = function () {
  const nameProduct = getEl("nameProduct").value;
  const description = getEl("description").value;
  const price = getEl("price").value;
  const id = getEl("idProduct").value;
  const type = getEl("type").value;
  const inventory = getEl("inventory").value;
  const image = getEl("imgLink").value;

  console.log(getEl("idProduct").value);

  var newProduct = new Product(
    nameProduct,
    description,
    price,
    type,
    inventory,
    image,
    id
  );

  var isValid = true;
  isValid &=
    checkRequired(nameProduct, "idName", "*Vui long nhap thong tin vao !") &&
    checkLength(nameProduct, "idName", 3, 20);

  isValid &=
    checkRequired(
      description,
      "errDescription",
      "*Vui long nhap thong tin vao !"
    ) && checkLength(description, "errDescription", 3, 50);

  isValid &=
    checkRequired(price, "errPrice", "*Vui long nhap thong tin vao !") &&
    checkLength(price, "errPrice", 1, 10) &&
    checkNumber(price, "errPrice");

  isValid &= checkRequired(type, "errType", "*Vui long nhap thong tin vao !");

  isValid &=
    checkRequired(
      inventory,
      "errInventory",
      "*Vui long nhap thong tin vao !"
    ) &&
    checkLength(inventory, "errInventory", 1, 4) &&
    checkNumber(inventory, "errInventory");

  isValid &=
    checkRequired(image, "errLink", "*Vui long nhap thong tin vao !") &&
    checkURL(image, "errLink");

  if (isValid) {
    productService
      .updateProduct(newProduct, id)
      .then(function (res) {
        alert("Cập Nhập thành Công !");
        getEl("txtCurd").innerHTML = "Add Product";
        getEl("addproduct").style.display = "block";
        getEl("updateproduct").style.display = "none";
        getEl("cancelUpdate").style.display = "none";
        fetchProductCrud();
        resetValue();
      })
      .catch(function (err) {
        console.log(err);
      });
  }
};
fetchProductCrud();
const cancelUp = function () {
  getEl("txtCurd").innerHTML = "Add Product";
  getEl("addproduct").style.display = "block";
  getEl("updateproduct").style.display = "none";
  getEl("cancelUpdate").style.display = "none";
};

// reset value add and update
const resetValue = function () {
  getEl("nameProduct").value = "";
  getEl("description").value = "";
  getEl("price").value = "";
  getEl("type").value = "";
  getEl("inventory").value = "";
  getEl("imgLink").value = "";
};

// -----------------VALIDATION______________________
const checkRequired = function (value, idError, error) {
  if (value) {
    getEl(idError).innerHTML = "";
    return true;
  } else {
    getEl(idError).innerHTML = error;
  }
};

const checkLength = function (value, iderror, minlength, maxlength) {
  if (value.length < minlength || value.length > maxlength) {
    getEl(
      iderror
    ).innerHTML = `Do dai ky tu phai tu  ${minlength} den ${maxlength} `;
    return false;
  } else {
    getEl(iderror).innerHTML = "";
    return true;
  }
};

const checkNumber = function (value, iderror) {
  const patternNumber = new RegExp("^[0-9]+$");
  if (patternNumber.test(value)) {
    getEl(iderror).innerHTML = "";
    return true;
  } else {
    getEl(iderror).innerHTML = "*Vui long nhap so ";
    return false;
  }
};

const checkURL = function (value, iderror) {
  const patternURL = new RegExp(/^(https?|chrome):\/\/[^\s$.?#].[^\s]*$/);
  if (patternURL.test(value)) {
    getEl(iderror).innerHTML = "";
    return true;
  } else {
    getEl(iderror).innerHTML = "*Vui long nhap vao mot url  ";
    return false;
  }
};
// console.log(ProductList);
// search Product
// const searchProduct = function () {
//   var result = [];
//   var keyword = document.getElementById("txtSearch").value;
//   console.log(keyword);

//   //lay từng thang trong ds nhân viên thằng  nweews có thì push nó vào result
//   //duyệt ds nvien,ktra từng nv1 ,nv nào đó có id giống với ketwword
//   for (var i = 0; i < ProductList.length; i++) {
//     var name = ProductList[i].name;
//     name = nonAccentVietnamese(name);
//     keyword = nonAccentVietnamese(keyword).trim();
//     if (name.indexOf(keyword) !== -1) {
//       result.push(ProductList[i]);
//     }
//   }
//   fetchProductCrud();
// };

function searchEmpl() {
  var result = [];
  var keyword = getEl("txtSearch").value;
  console.log(ProductList);
  //lay từng thang trong ds nhân viên thằng  nweews có thì push nó vào result
  //duyệt ds nvien,ktra từng nv1 ,nv nào đó có id giống với ketwword
  for (var i = 0; i < ProductList.length; i++) {
    if (ProductList[i].id === keyword) {
      // result = [];
      result.push(ProductList[i]);
      break;
    }
    var fullname = ProductList[i].name;

    //convert ketword

    fullname = nonAccentVietnamese(fullname);
    keyword = nonAccentVietnamese(keyword).trim();

    //chuyển vê chử thường
    if (fullname.indexOf(keyword) !== -1) {
      result.push(ProductList[i]);
    }
  }
  //input : keyword
  renderCurdTable(result);

  //expectation: result
}

//convert vn sang en
function nonAccentVietnamese(str) {
  str = str.toLowerCase();
  //     We can also use this instead of from line 11 to line 17
  //     str = str.replace(/\u00E0|\u00E1|\u1EA1|\u1EA3|\u00E3|\u00E2|\u1EA7|\u1EA5|\u1EAD|\u1EA9|\u1EAB|\u0103|\u1EB1|\u1EAF|\u1EB7|\u1EB3|\u1EB5/g, "a");
  //     str = str.replace(/\u00E8|\u00E9|\u1EB9|\u1EBB|\u1EBD|\u00EA|\u1EC1|\u1EBF|\u1EC7|\u1EC3|\u1EC5/g, "e");
  //     str = str.replace(/\u00EC|\u00ED|\u1ECB|\u1EC9|\u0129/g, "i");
  //     str = str.replace(/\u00F2|\u00F3|\u1ECD|\u1ECF|\u00F5|\u00F4|\u1ED3|\u1ED1|\u1ED9|\u1ED5|\u1ED7|\u01A1|\u1EDD|\u1EDB|\u1EE3|\u1EDF|\u1EE1/g, "o");
  //     str = str.replace(/\u00F9|\u00FA|\u1EE5|\u1EE7|\u0169|\u01B0|\u1EEB|\u1EE9|\u1EF1|\u1EED|\u1EEF/g, "u");
  //     str = str.replace(/\u1EF3|\u00FD|\u1EF5|\u1EF7|\u1EF9/g, "y");
  //     str = str.replace(/\u0111/g, "d");
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  // Some system encode vietnamese combining accent as individual utf-8 characters
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng
  str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư
  return str;
}

const mapData = function (data) {
  ProductList = [];
  for (var i = 0; i < data.length; i++) {
    const newProduct = new Product(
      data[i].name,
      data[i].description,
      data[i].price,
      data[i].type,
      data[i].inventory,
      data[i].image,
      data[i].id
    );
    ProductList.push(newProduct);
  }
};

// get Ele
const getEl = function (id) {
  return document.getElementById(id);
};
