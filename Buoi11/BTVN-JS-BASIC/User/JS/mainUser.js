var ProductList = [];
var BuyProductList = [];
var newProductService = new ProductService();
var idInput = -1;

// hiển thi danh sách
const fetchProduct = function () {
  newProductService
    .getListProduct()
    .then(function (res) {
      mapData(res.data);

      renderProduct();
    })
    .catch(function (err) {
      console.log(err);
    });
};

fetchProduct();

//Render data product
const renderProduct = function (data) {
  data = data || ProductList;

  var htmlContent = "";
  for (var i = 0; i < data.length; i++) {
    htmlContent += `
          <div class="card">
          <div class="imgBox">
            <img src="${data[i].image}" />
          </div>
          <div class="details">
            <div class="textContent">
              <h3>${data[i].name}</h3>
              <div class="price">${data[i].price}$ </div>
            </div>
            <h4>Colors</h4>
            <ul>
              <li></li>
              <li></li>
              <li></li>
              <li></li>
              <li></li>
            </ul>
            <button style="outline: none" onclick="buyProduct(${data[i].id})">Buy It Now</button>
          </div>
  
          <div class="description">
            <div class="icon"><i class="fas fa-info-circle"></i></div>
            <div class="contents">
              <h2>Description</h2>
              <p>
              ${data[i].description}
              </p>
            </div>
          </div>
        </div>
          `;
  }
  if (getEl("content-card")) {
    getEl("content-card").innerHTML = htmlContent;
  }
};

//hiểu roài
// do là thế này
/// nếu tại trang index thì

// Map data product
const mapData = function (data) {
  ProductList = [];
  for (var i = 0; i < data.length; i++) {
    const newProduct = new Product(
      data[i].name,
      data[i].description,
      data[i].price,
      data[i].type,
      data[i].inventory,
      data[i].image,
      data[i].id
    );
    ProductList.push(newProduct);
  }
};

// const mapDataBuy = function (data) {
//   BuyProductList = [];
//   for (var i = 0; i < data.length; i++) {
//     const newCartItem = new CartItem(
//       data[i].names,
//       data[i].price,
//       data[i].type,
//       data[i].inventory,
//       data[i].image,
//       data[i].id
//     );
//     BuyProductList.push(newCartItem);
//   }
//   console.log(BuyProductList);
// };

// --------------------------------SORT----------------------------------------

//Sort Product
//sort tang daan
const sortAscendingPrice = function (data) {
  var temp;
  for (var i = data.length; i > 0; i--) {
    for (var j = 0; j < i - 1; j++) {
      if (+data[j].price > +data[j + 1].price) {
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }
  return data;
};

// so sánh kí tự
const sortAscendingName = function (data) {
  var temp;
  for (var i = data.length; i > 0; i--) {
    for (var j = 0; j < i - 1; j++) {
      if (data[j].name > data[j + 1].name) {
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }
  return data;
};

//sort giam dan
const sortDecreasePrice = function (data) {
  var temp;
  for (var i = data.length; i > 0; i--) {
    for (var j = 0; j < i - 1; j++) {
      if (+data[j].price < +data[j + 1].price) {
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }
  return data;
};
// so sánh kí tự
const sortDecreaseName = function (data) {
  var temp;
  for (var i = data.length; i > 0; i--) {
    for (var j = 0; j < i - 1; j++) {
      console.log(j);
      if (data[j].name < data[j + 1].name) {
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }
  return data;
};
const sortSelect = function () {
  var sort = document.getElementById("mySelect").value;
  if (sort == 1) {
    return renderProduct(sortAscendingPrice(ProductList));
  }
  if (sort == 2) {
    return renderProduct(sortDecreasePrice(ProductList));
  }
  if (sort == 3) {
    return renderProduct(sortAscendingName(ProductList));
  }
  if (sort == 4) {
    return renderProduct(sortDecreaseName(ProductList));
  }
};
//--------------------------------ENDSORT----------------------------------------

// ---------------------------------SEARCH__PRODUCT---------------------------
function searchEmpl() {
  var result = [];
  var keyword = getEl("txtSearch").value;

  //lay từng thang trong ds nhân viên thằng  nweews có thì push nó vào result
  //duyệt ds nvien,ktra từng nv1 ,nv nào đó có id giống với ketwword
  for (var i = 0; i < ProductList.length; i++) {
    if (ProductList[i].id === keyword) {
      // result = [];
      result.push(ProductList[i]);
      break;
    }
    var fullname = ProductList[i].name;

    //convert ketword

    fullname = nonAccentVietnamese(fullname);
    keyword = nonAccentVietnamese(keyword).trim();

    //chuyển vê chử thường
    if (fullname.indexOf(keyword) !== -1) {
      result.push(ProductList[i]);
    }
  }
  //input : keyword
  renderProduct(result);

  //expectation: result
}

//convert vn sang en
function nonAccentVietnamese(str) {
  str = str.toLowerCase();
  //     We can also use this instead of from line 11 to line 17
  //     str = str.replace(/\u00E0|\u00E1|\u1EA1|\u1EA3|\u00E3|\u00E2|\u1EA7|\u1EA5|\u1EAD|\u1EA9|\u1EAB|\u0103|\u1EB1|\u1EAF|\u1EB7|\u1EB3|\u1EB5/g, "a");
  //     str = str.replace(/\u00E8|\u00E9|\u1EB9|\u1EBB|\u1EBD|\u00EA|\u1EC1|\u1EBF|\u1EC7|\u1EC3|\u1EC5/g, "e");
  //     str = str.replace(/\u00EC|\u00ED|\u1ECB|\u1EC9|\u0129/g, "i");
  //     str = str.replace(/\u00F2|\u00F3|\u1ECD|\u1ECF|\u00F5|\u00F4|\u1ED3|\u1ED1|\u1ED9|\u1ED5|\u1ED7|\u01A1|\u1EDD|\u1EDB|\u1EE3|\u1EDF|\u1EE1/g, "o");
  //     str = str.replace(/\u00F9|\u00FA|\u1EE5|\u1EE7|\u0169|\u01B0|\u1EEB|\u1EE9|\u1EF1|\u1EED|\u1EEF/g, "u");
  //     str = str.replace(/\u1EF3|\u00FD|\u1EF5|\u1EF7|\u1EF9/g, "y");
  //     str = str.replace(/\u0111/g, "d");
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  // Some system encode vietnamese combining accent as individual utf-8 characters
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng
  str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư
  return str;
}

//---------------------------------END-SEARCH-PRODUCT-------------------

// --------------------------------

/** *********************************CART PRODYCT ************************* */

const buyProduct = function (id) {
  // const id = ProductList[i].id;
  // const name = ProductList[i].name;
  // const price = ProductList[i].price;
  // const type = ProductList[i].type;
  // const inventory = ProductList[i].inventory;
  // const image = ProductList[i].image;

  const index = findById(id);
  //finbyid buyproducrt
  const indexD = findByIdDelete(id);
  if (indexD !== -1) {
    const name = BuyProductList[indexD].name;
    const price = BuyProductList[indexD].price;
    const type = BuyProductList[indexD].type;
    const inventory = BuyProductList[indexD].inventory;
    const image = BuyProductList[indexD].image;
    const soLuong = BuyProductList[indexD].soLuong + 1;
    const updateCartItem = new CartItem(
      name,
      price,
      type,
      inventory,
      image,
      id,
      soLuong
    );
    BuyProductList[indexD] = updateCartItem;
  } else {
    //neu đẫ có trong giỏ hàng thì sẽ có index => product[index].sl +1
    if (index !== -1) {
      const name = ProductList[index].name;
      const price = ProductList[index].price;
      const type = ProductList[index].type;
      const inventory = ProductList[index].inventory;
      const image = ProductList[index].image;
      const newCartItem = new CartItem(
        name,
        price,
        type,
        inventory,
        image,
        id,
        1
      );
      BuyProductList.push(newCartItem);
    } else {
      console.log("Opps !!!! Something went wrong !");
    }
  }

  alert("Đã thêm vào giỏ hàng thành công !");
  saveData(BuyProductList);
};
// tui k bieet cacsh lay id no ra noi

const renderBuyProduct = function (data) {
  data = data || BuyProductList;
  var htmlContentBuy = "";
  total = 0;
  for (var i = 0; i < data.length; i++) {
    total += data[i].price * data[i].soLuong;
    htmlContentBuy += ` <div class="cart__content__product__item container  mb-3">
    <div class="row align-items-center">
        <div class="col-11 cart__content__product__item__cont">
            <div class="row align-items-center">
                <img src="${data[i].image}" style="height: 100px; width: 100px;" alt="" class="col-2">
                <div class="col-3 name__product" ">
                    <p style="margin-bottom: 0; font-size:20px; font-weight: 600;">${data[i].name}</p>
                    <p  style="margin-bottom: 0;">Rating: 10</p>
                </div>
                <div class="col-2"><p  style="margin-bottom: 0; font-size: 20px; font-weight: 600;">${data[i].type}</p></div>
                <div class="col-2 input__them__bot"><span class="product__reduction" onclick="handleSoLuong(${data[i].id},'tru')">-</span><input type="text" class="input__increase" name=""  value="${data[i].soLuong}"><span onclick="handleSoLuong(${data[i].id},'cong')" class="increase__product">+</span></div>
                <div class="col-3"><span class="price__product">${data[i].price}$</span></div>
            </div>
            

        </div>
        <div class="col "  ><i class='bx bx-x-circle cancel-icon' onclick="deleteProductCart(${data[i].id})"></i></div>

    </div>
</div>
   
    `;

    if (document.getElementById("total-money")) {
      document.getElementById("total-money").innerHTML = total + "$";
    }
  }
  ///lamg sao ddeer lay id cuar tung cai input rooi truyen cho cai tang so luong ong he'
  if (document.getElementById("cartProduct")) {
    document.getElementById("cartProduct").innerHTML = htmlContentBuy;
  }
};

//check id
const findById = function (idbuy) {
  for (var i = 0; i < ProductList.length; i++) {
    if (parseInt(ProductList[i].id) === parseInt(idbuy)) {
      return i;
    }
  }
  return -1;
};

const findByIdDelete = function (id) {
  for (var i = 0; i < BuyProductList.length; i++) {
    if (parseInt(BuyProductList[i].id) === parseInt(id)) {
      return i;
    }
  }
  return -1;
};

//đợi xíu

// save Data
const saveData = function (data) {
  console.log(JSON.stringify(data));

  localStorage.setItem("ProductCart", JSON.stringify(data));
};
//Xóa sphan trong gio han
const deleteProductCart = function (id) {
  const index = findByIdDelete(id);
  if (index !== -1) {
    BuyProductList.splice(index, 1);

    alert("xóa sản phẩm thành công !");
    renderBuyProduct(BuyProductList);
    saveData(BuyProductList);
  } else {
    alert("Opps !!!! Something went wrong !");
  }
};

function fetchData() {
  const localCartList = localStorage.getItem("ProductCart");
  //kiem tra ton tai
  if (localCartList) {
    //chuyển lại thành chuổi object
    // employeeList = JSON.parse(localEmplList);

    BuyProductList = JSON.parse(localCartList);
    //console.log(BuyProductList);
    //in danh sach hien tai ra màn hình

    renderBuyProduct();
  }
}
fetchData();
//hỏi dụ gì
//render cái hàm renderBuyProduct(); ní k chạy sản phẩm
//

//  them bot so luong product

function handleSoLuong(id, thaoTac) {
  let index = findByIdDelete(id);

  if (thaoTac == "cong") {
    //neees tang soLuong
    BuyProductList[index].soLuong += 1;
    console.log(BuyProductList[index].soLuong);
  } else {
    //neu giam soLuong

    BuyProductList[index].soLuong -= 1;
    console.log(BuyProductList[index].soLuong);
  }
  renderBuyProduct();
}

// function adds(id) {
//   var foo = +document.getElementById(id).value;
//   foo++;

//   document.getElementById(id).value = foo;
//   //ham o day
//   //ok
// }
// function trus(id) {
//   var foo = +document.getElementById(id).value;
//   if (foo > 1) {
//     foo--;

//     document.getElementById(id).value = foo;
//   }
// }

// black

// mua hang thanh cong
const checkOut = function () {
  BuyProductList = [];
  alert("Mua hàng thành công !");
  document.getElementById("total-money").innerHTML = "0$";
  renderBuyProduct(BuyProductList);
};
function goBack() {
  window.history.back();
}
//---------------------------------------------------------------
const getEl = function (id) {
  return document.getElementById(id);
};
