/**
 * Người viết : Đức Mạnh
 * Ngày : 9/15/2020
 * Công việt Tạo 1 website bán hàng
 */
var ProductList = [];
var productService = new  ProductService();
var idProduct = -1;

//hiển thi danh sách
const fetchProduct = function () {
  productService
    .getListProduct()
    .then(function (res) {
      ProductList = res.data;
      //   console.log(ProductList);
      // renderCurdTable();
      renderProduct();
    })
    .catch(function (err) {
      console.log(err);
    });
};


//Render data product
const renderProduct = function () {
  var htmlContent = "";
  for (var i = 0; i < ProductList.length; i++) {
    htmlContent += `
        <div class="card">
        <div class="imgBox">
          <img src="${ProductList[i].image}" />
        </div>
        <div class="details">
          <div class="textContent">
            <h3>${ProductList[i].name}</h3>
            <div class="price">${ProductList[i].price}$ </div>
          </div>
          <h4>Colors</h4>
          <ul>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
          <button>Buy It Now</button>
        </div>

        <div class="description">
          <div class="icon"><i class="fas fa-info-circle"></i></div>
          <div class="contents">
            <h2>Description</h2>
            <p>
            ${ProductList[i].description}
            </p>
          </div>
        </div>
      </div>
        `;

    getEl("content-card").innerHTML = htmlContent;
  }
};

// --------------------------------
fetchProduct();

// Crud load
const fetchProductCrud = function () {
  productService
    .getListProduct()
    .then(function (res) {
      ProductList =res.data;
      renderCurdTable();

      // renderProduct();
    })
    .catch(function (err) {
      console.log(err);
    });
};
fetchProductCrud();
// render table CUrd
const renderCurdTable = function () {
  htmlContents = "";
  for (var i = 0; i < ProductList.length; i++) {
    htmlContents += `
          <tr>
          <td>  ${i + 1}</td>
          <td>  ${ProductList[i].name}</td>
          <td>${ProductList[i].description}</td>
          <td>${ProductList[i].price}</td>
          <td>${ProductList[i].type}</td>
          <td>${ProductList[i].inventory}</td>
          <td><img class="img-fluid" src="${
            ProductList[i].image
          }" alt="" style="height: 30px; width: 30px;"></td>
          <td>
              <button class="btn btn-danger" onclick="DeleteProduct( ${
                ProductList[i].id
              })"><i class='bx bx-trash' ></i></button>
              <button class="btn btn-primary" onclick="getIfromationProduct( ${
                ProductList[i].id
              })"><i class='bx bxs-pencil' ></i></button>
          </td>
      </tr>
          `;
    getEl("tbodyCrud").innerHTML = htmlContents;
  }
  //   console.log("table", htmlContents);
};




// Theem nhan vien
const AddProduct = function () {
  const nameProduct = getEl("nameProduct").value;
  const description = getEl("description").value;
  const price = getEl("price").value;
  const type = getEl("type").value;
  const inventory = getEl("inventory").value;
  const image = getEl("imgLink").value;
  var newProduct = new Product(
    nameProduct,
    description,
    price,
    type,
    inventory,
    image
  );

  var isValid = true;
  isValid &=
    checkRequired(nameProduct, "idName", "*Vui long nhap thong tin vao !") &&
    checkLength(nameProduct, "idName", 3, 20);

  isValid &=
    checkRequired(
      description,
      "errDescription",
      "*Vui long nhap thong tin vao !"
    ) && checkLength(description, "errDescription", 3, 50);

  isValid &=
    checkRequired(price, "errPrice", "*Vui long nhap thong tin vao !") &&
    checkLength(price, "errPrice", 1, 10) &&
    checkNumber(price, "errPrice");

  isValid &= checkRequired(type, "errType", "*Vui long nhap thong tin vao !");

  isValid &=
    checkRequired(
      inventory,
      "errInventory",
      "*Vui long nhap thong tin vao !"
    ) &&
    checkLength(inventory, "errInventory", 1, 4) &&
    checkNumber(inventory, "errInventory");

    isValid &=  checkRequired(image, "errLink", "*Vui long nhap thong tin vao !") &&
    checkURL(image, "errLink");

  if (isValid) {
    productService
      .addProduct(newProduct)
      .then(function (res) {
        alert("Thêm thành công !");

        fetchProduct();
        fetchProductCrud();
        resetValue();

        //   console.log(newProduct);
      })
      .catch(function (err) {
        console.log(err);
      });
    console.log("hello");
  }
};
//Xóa product
const DeleteProduct = function (id) {
 productService.deleteProduct(id)
    .then(function (res) {
      alert("xoa nguoi dung thanh cong !");
      fetchProduct();
      fetchProductCrud();
    })
    .catch(function (err) {
      console.log(err);
    });
};

// -----------------------------------------------

// get product information
const getIfromationProduct = function (id) {
  idProduct = id;
  console.log(idProduct);
  getEl("txtCurd").innerHTML = "Update Product";
  getEl("addproduct").style.display = "none";
  getEl("updateproduct").style.display = "block";
  getEl("cancelUpdate").style.display = "block";
  axios({
    url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
    method: "GET",
    data: null,
  })
    .then(function (res) {
      getEl("nameProduct").value = res.data.name;
      getEl("description").value = res.data.description;
      getEl("price").value = res.data.price;
      getEl("type").value = res.data.type;
      getEl("inventory").value = res.data.inventory;
      getEl("imgLink").value = res.data.image;
    })
    .catch(function (err) {
      console.log(err);
    });
};

// Update Product
const updateProduct = function () {
  const nameProduct = getEl("nameProduct").value;
  const description = getEl("description").value;
  const price = getEl("price").value;
  const type = getEl("type").value;
  const inventory = getEl("inventory").value;
  const image = getEl("imgLink").value;

  var newProduct = new Product(
    nameProduct,
    description,
    price,
    type,
    inventory,
    image
  );

  var isValid = true;
  isValid &=
    checkRequired(nameProduct, "idName", "*Vui long nhap thong tin vao !") &&
    checkLength(nameProduct, "idName", 3, 20);

  isValid &=
    checkRequired(
      description,
      "errDescription",
      "*Vui long nhap thong tin vao !"
    ) && checkLength(description, "errDescription", 3, 50);

  isValid &=
    checkRequired(price, "errPrice", "*Vui long nhap thong tin vao !") &&
    checkLength(price, "errPrice", 1, 10) &&
    checkNumber(price, "errPrice");

  isValid &= checkRequired(type, "errType", "*Vui long nhap thong tin vao !");

  isValid &=
    checkRequired(
      inventory,
      "errInventory",
      "*Vui long nhap thong tin vao !"
    ) &&
    checkLength(inventory, "errInventory", 1, 4) &&
    checkNumber(inventory, "errInventory");

    isValid &=  checkRequired(image, "errLink", "*Vui long nhap thong tin vao !") &&
    checkURL(image, "errLink");

 if(isValid){
  getEl("txtCurd").innerHTML = "Add Product";
  getEl("addproduct").style.display = "block";
  getEl("updateproduct").style.display = "none";
  getEl("cancelUpdate").style.display = "none";

  productService
    .updateProduct(newProduct, idProduct)
    .then(function (res) {
      alert("Cập Nhập thành Công !");
      fetchProduct();
      fetchProductCrud();
      resetValue();
    })
    .catch(function (err) {
      console.log(err);
    });
 }
};

const cancelUp = function(){
  getEl("txtCurd").innerHTML = "Add Product";
  getEl("addproduct").style.display = "block";
  getEl("updateproduct").style.display = "none";
  getEl("cancelUpdate").style.display = "none";
}

// reset value add and update
const resetValue = function () {
  getEl("nameProduct").value = "";
  getEl("description").value = "";
  getEl("price").value = "";
  getEl("type").value = "";
  getEl("inventory").value = "";
  getEl("imgLink").value = "";
};

// -----------------VALIDATION______________________
const checkRequired = function (value, idError, error) {
  if (value) {
    getEl(idError).innerHTML = "";
    return true;
  } else {
    getEl(idError).innerHTML = error;
  }
};

const checkLength = function (value, iderror, minlength, maxlength) {
  if (value.length < minlength || value.length > maxlength) {
    getEl(
      iderror
    ).innerHTML = `Do dai ky tu phai tu  ${minlength} den ${maxlength} `;
    return false;
  } else {
    getEl(iderror).innerHTML = "";
    return true;
  }
};

const checkNumber = function (value, iderror) {
  const patternNumber = new RegExp("^[0-9]+$");
  if (patternNumber.test(value)) {
    getEl(iderror).innerHTML = "";
    return true;
  } else {
    getEl(iderror).innerHTML = "*Vui long nhap so ";
    return false;
  }
};

const checkURL = function (value, iderror) {
  const patternURL = new RegExp(/^(https?|chrome):\/\/[^\s$.?#].[^\s]*$/);
  if (patternURL.test(value)) {
    getEl(iderror).innerHTML = "";
    return true;
  } else {
    getEl(iderror).innerHTML = "*Vui long nhap vao mot url  ";
    return false;
  }
};
// get Ele
const getEl = function (id) {
  return document.getElementById(id);
};
