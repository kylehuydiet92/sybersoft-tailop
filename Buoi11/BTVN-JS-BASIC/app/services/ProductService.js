function ProductService(){
    this.getListProduct= function(){
        return  axios({
            url:'https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products',
            method:"GET",
            data: null,
        });
    }

    this.addProduct = function(data){
        return    axios({
            url:'https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products',
            method:'POST',
            data:data
        })
    }
    
    this.deleteProduct = function(id){
        return  axios({
            url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
            method: "DELETE",
            data: null,
          })
    }

    this.updateProduct =function(data,id){
    return  axios({
            url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
            method: "PUT",
            data: data,
          })
    }
}