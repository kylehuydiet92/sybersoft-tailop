var ndService = new NguoiDungService();

// function call lấy API
function layDanhSachND() {
  // axios({
  //   url: "https://5f5442d0e5de110016d51e71.mockapi.io/users",
  //   method: "GET",
  //   data: null,
  // })
  ndService
    .getListUser()
    .then(function thanhCong(res) {
      console.log(res);
      hienThiTabel(res.data);
    })
    .catch(function thatBai(err) {
      console.log(err);
    });
}

// funciton render
function hienThiTabel(listUser) {
  var htmlContent = "";
  listUser.forEach((users, index) => {
    htmlContent += `
            <tr>
                <td>${index + 1}</td>
           
                <td>${users.taikhoan}</td>
                <td>${users.matkhau}</td>
                <td>${users.hoten}</td>
                <td>${users.sdt}</td>
                <td>${users.email}</td>
                <td>${users.loaind}</td>
                <td>
                  <button class="btn btn-danger" onclick="xoaND(${
                    users.id
                  })"><i class="fa fa-trash"></i></button>
                  <button class="btn btn-success" data-toggle="modal" data-target="#myModal" 
                  onclick=updateND(${users.id})>
                  <i class="fa fa-paint-brush"></i>
                  </button>
                </td>
          
            </tr>
        `;
  });
  getEl("tblDanhSachNguoiDung").innerHTML = htmlContent;
}

layDanhSachND();
//axios return promise  có 3trạng thái(3)
/**
 * tráng thái thứ 1 : pendding(chờ)
 *  tráng thái thứ 2: resolve(thành công)
 *      đẻ giải quết thành công : .then()
 *   tráng thái thứ 3 : reject(thất bại)
 *      để giải quyết thất bại .catch
 */

// sử lý model

getEl("btnThemNguoiDung").addEventListener("click", function () {
  document.getElementsByClassName("modal-title")[0].innerHTML =
    "Them nhan vien moi";
  document.getElementsByClassName("modal-footer")[0].innerHTML =
    '<button class=" btn btn-success" onclick="themND()">Them nhan vien</button>';
});

//them nguoi dung
function themND() {
  // console.log("run");
  const taikhoan = getEl("TaiKhoan").value;
  const hoten = getEl("HoTen").value;
  const matkhau = getEl("MatKhau").value;
  const email = getEl("Email").value;
  const sdt = getEl("SoDienThoai").value;
  const loaind = getEl("loaiNguoiDung").value;
  var nguoiDung = new NguoiDung(taikhoan, hoten, matkhau, email, sdt, loaind);
  // axios({
  //   url: "https://5f5442d0e5de110016d51e71.mockapi.io/users",
  //   method: "POST",
  //   data: nguoiDung,
  // })
  var isValid = true;
  isValid &=
    checkRequired(taikhoan, "taikhoanHelp", "Vui lòng nhập tài khoản !.") &&
    checkLength(taikhoan, "taikhoanHelp", 8, 30);
  isValid &=
    checkRequired(hoten, "hotenHelp", "Vui lòng nhập họ tên !.") &&
    checkString(hoten, "hotenHelp");
  isValid &=
    checkRequired(matkhau, "passwordHelp", "Vui lòng nhập mật khẩu !.") &&
    checkLength(matkhau, "passwordHelp", 6, 30);
  isValid &=
    checkRequired(email, "emailHelp", "Vui lòng nhập email !.") &&
    checkEmail(email, "emailHelp");
  isValid &=
    checkRequired(sdt, "sdtHelp", "Vui lòng nhập sdt !.") &&
    checkLength(sdt, "sdtHelp", 10, 11);

 if(isValid){
  ndService
  .addUser(nguoiDung)
  .then(function thanhCong(res) {

    alert("them nguoi dung thanh cong");
    layDanhSachND();
    resetValue();
  })
  .catch(function thatBai(err) {

    console.log(err);
  });
//console.log("nguoi dung: ",nguoiDung)
 }
}

function getEl(id) {
  return document.getElementById(id);
}

//Xoas nguoi dung
function xoaND(id) {
  console.log(id);

  // axios({
  //   url: `https://5f5442d0e5de110016d51e71.mockapi.io/users/${id}`,
  //   method: "DELETE",
  //   data: null,
  // })
  ndService
    .deleteUser(id)
    .then(function (res) {
      alert("xoa nguoi dung thanh cong !");
      layDanhSachND();
    })
    .catch(function (err) {
      console.log(err);
    });
}

//suwax nugowif dung
function updateND(id) {
  document.getElementsByClassName("modal-title")[0].innerHTML = "Sua nhan vien";
  document.getElementsByClassName(
    "modal-footer"
  )[0].innerHTML = `<button class=" btn btn-success " onclick="capnhapND(${id})">Cap nhat nhan vien</button>`;

  //call api
  // axios({
  //   url: `https://5f5442d0e5de110016d51e71.mockapi.io/users/${id}`,
  //   method: "GET",
  //   data: null,
  // })
  ndService
    .getDetailUser(id)
    .then(function (res) {
      console.log(res.data.tenloai);
      getEl("TaiKhoan").value = res.data.taikhoan;
      getEl("HoTen").value = res.data.hoten;
      getEl("MatKhau").value = res.data.matkhau;
      getEl("Email").value = res.data.email;
      getEl("SoDienThoai").value = res.data.sdt;
      getEl("loaiNguoiDung").value = res.data.loaind;
    })
    .catch(function (err) {
      console.log(err);
    });
}

function capnhapND(id) {
  const taikhoan = getEl("TaiKhoan").value;
  const hoten = getEl("HoTen").value;
  const matkhau = getEl("MatKhau").value;
  const email = getEl("Email").value;
  const sdt = getEl("SoDienThoai").value;
  const loaind = getEl("loaiNguoiDung").value;
  var nguoiDung = new NguoiDung(taikhoan, hoten, matkhau, email, sdt, loaind);

  // axios({
  //   url: `https://5f5442d0e5de110016d51e71.mockapi.io/users/${id}`,
  //   method: "PUT",
  //   data: nguoiDung,
  // })
  ndService
    .updateUser(id, nguoiDung)
    .then(function (res) {
      alert("Cap nhap nguoi dung thanh cong");

      layDanhSachND();
      resetValue();
    })
    .catch(function (err) {
      console.log(err);
    });
}
function resetValue() {
  document.getElementById("TaiKhoan").value = "";
  document.getElementById("HoTen").value = "";
  document.getElementById("MatKhau").value = "";
  document.getElementById("Email").value = "";
  document.getElementById("SoDienThoai").value = "";
}
// valiable
function checkRequired(value, idError, error) {
  if (value) {
    getEl(idError).innerHTML = "";
    return true;
  } else {
    getEl(idError).innerHTML = error;
  }
}
//kiểm tra chiêu dài chuôi
function checkLength(value, idError, minlength, maxlength) {
  if (value.length < minlength || value.length > maxlength) {
    getEl(idError).innerHTML = `Độ dài phải từ ${minlength} đến ${maxlength} `;
    return false;
  } else {
    getEl(idError).innerHTML = "";
    return true;
  }
}
// kiểm tra tiếng việt
function checkString(value, idError) {
  const pattern = new RegExp(
    "^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêếìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶ" +
      "ẸẺẼỀẾỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợ" +
      "ụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$"
  );
  if (pattern.test(value)) {
    getEl(idError).innerHTML = "";
    return true;
  } else {
    getEl(idError).innerHTML = "*Dữ liêu không đúng định dạng !";
    return false;
  }
}
//kiểm tra email
function checkEmail(value, idError) {
  const pattern = new RegExp(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
  if (pattern.test(value)) {
    getEl(idError).innerHTML = "";
    return true;
  } else {
    getEl(idError).innerHTML = "*Email không đúng định dạng !";
    return false;
  }
}
