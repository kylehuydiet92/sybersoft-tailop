function NguoiDungService(){
    this.getListUser = function(){
     return   axios({
            url: "https://5f5442d0e5de110016d51e71.mockapi.io/users",
            method: "GET",
            data: null,
          });
    };

    this.addUser =function(users){
        return  axios({
            url: "https://5f5442d0e5de110016d51e71.mockapi.io/users",
            method: "POST",
            data: users,
          });
    };

    this.deleteUser = function(id){
        return axios({
            url: `https://5f5442d0e5de110016d51e71.mockapi.io/users/${id}`,
            method: "DELETE",
            data: null,
          });
    };

    this.getDetailUser = function(id){
        return  axios({
            url: `https://5f5442d0e5de110016d51e71.mockapi.io/users/${id}`,
            method: "GET",
            data: null,
          });
    };

    this.updateUser = function(id,users){
        return axios({
            url: `https://5f5442d0e5de110016d51e71.mockapi.io/users/${id}`,
            method: "PUT",
            data: users,
          });
    };
}