var ndService = new NguoiDungService();

function layDanhSachND() {
  //   axios({
  //     url: "https://5f544347e5de110016d51ecc.mockapi.io/users",
  //     method: "GET",
  //     data: null,
  //   })
  ndService
    .getListUser()
    .then(function thanhCong(res) {
      console.log(res);
      hienThiTable(res.data);
    })
    .catch(function thatBai(err) {
      console.log(err);
    });

  /**
   *axios return promise  ( 3 ):
   *  pendding ( chờ )
   *      resolve ( thành công )
   *           .then()
   *      reject ( thất bại )
   *            .catch
   */
}

function hienThiTable(listUser) {
  var content = "";
  listUser.forEach((user, index) => {
    content += `
            <tr>
            <td>${index + 1}</td>
                <td>${user.taiKhoan}</td>
                <td>${user.matKhau}</td>
                <td>${user.hoTen}</td>
                <td>${user.soDT}</td>
                <td>${user.email}</td>
                <td>${user.tenLoai}</td>
                <td>
                    <button class="btn btn-danger"
                     onclick="xoaND(${user.id})">
                        xóa
                    </button>
                    <button class="btn btn-success" data-toggle="modal" data-target="#myModal" onclick="suaND(${
                      user.id
                    })">
                        sửa
                    </button>
                </td>
            </tr>
        `;
  });
  document.getElementById("tblDanhSachNguoiDung").innerHTML = content;
}

layDanhSachND();

// xử lý modal
document
  .getElementById("btnThemNguoiDung")
  .addEventListener("click", function () {
    document.getElementsByClassName("modal-title")[0].innerHTML =
      "Thêm Mới Người Dùng";
    document.getElementsByClassName("modal-footer")[0].innerHTML = `
        <button class="btn btn-info" onclick="themND()">Thêm Mới Người Dùng</button>
    `;
  });

function themND() {
  console.log("run");
  var taiKhoan = document.getElementById("TaiKhoan").value;
  var hoTen = document.getElementById("HoTen").value;
  var matKhau = document.getElementById("MatKhau").value;
  var email = document.getElementById("Email").value;
  var soDT = document.getElementById("SoDienThoai").value;
  var tenLoai = document.getElementById("loaiNguoiDung").value;

  var nguoiDung = new NguoiDung(taiKhoan, matKhau, hoTen, email, soDT, tenLoai);


  // axios({
  //   url: "https://5f544347e5de110016d51ecc.mockapi.io/users",
  //   method: "POST",
  //   data: nguoiDung,
  // })
  ndService
    .addUser(nguoiDung)
    .then(function thanhCong(res) {
      alert("đã thêm Người dùng thành công");
      
      layDanhSachND();
    })
    .catch(function thatBai(err) {
      console.log(err);
    });


}

// xóa người dùng

function xoaND(id) {
  console.log(id);

  // axios({
  //   url: `https://5f544347e5de110016d51ecc.mockapi.io/users/${id}`,
  //   method: "DELETE",
  //   data: null,
  // })
  ndService
    .deleteUser(id)
    .then(function (res) {
      alert("xóa Người dùng thành công");
      layDanhSachND();
    })
    .catch(function (err) {
      console.log(err);
    });
}

// sửa người dùng
function suaND(id) {
  console.log(id);

  // xử lý modal
  document.getElementsByClassName("modal-title")[0].innerHTML =
    "Sửa Người Dùng";
  document.getElementsByClassName("modal-footer")[0].innerHTML = `
        <button class="btn btn-info" onclick="capNhatND(${id})">Cập Nhật Người Dùng</button>
    `;

  // call api
  // axios({
  //   url: `https://5f544347e5de110016d51ecc.mockapi.io/users/${id}`,
  //   method: "GET",
  //   data: null,
  // })
  ndService
    .getDetailUser(id)
    .then(function (res) {
      document.getElementById("TaiKhoan").value = res.data.taiKhoan;
      document.getElementById("HoTen").value = res.data.hoTen;
      document.getElementById("MatKhau").value = res.data.matKhau;
      document.getElementById("Email").value = res.data.email;
      document.getElementById("SoDienThoai").value = res.data.soDT;
      document.getElementById("loaiNguoiDung").value = res.data.tenLoai;
    })
    .catch(function (err) {
      console.log(err);
    });
}

// cập nhật người dùng
function capNhatND(id) {
  console.log(id);

  var taiKhoan = document.getElementById("TaiKhoan").value;
  var hoTen = document.getElementById("HoTen").value;
  var matKhau = document.getElementById("MatKhau").value;
  var email = document.getElementById("Email").value;
  var soDT = document.getElementById("SoDienThoai").value;
  var tenLoai = document.getElementById("loaiNguoiDung").value;

  var nguoiDung = new NguoiDung(taiKhoan, matKhau, hoTen, email, soDT, tenLoai);

  // axios({
  //   url: `https://5f544347e5de110016d51ecc.mockapi.io/users/${id}`,
  //   method: "PUT",
  //   data: nguoiDung,
  // })
  ndService
    .updateUser(id, nguoiDung)
    .then(function (res) {
      alert("cập nhật người dùng thành công");
      layDanhSachND();
    })
    .catch(function (err) {
      console.log(err);
    });
}
