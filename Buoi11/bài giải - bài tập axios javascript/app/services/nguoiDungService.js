function NguoiDungService() {
  this.getListUser = function () {
    return axios({
      url: "https://5f544347e5de110016d51ecc.mockapi.io/users",
      method: "GET",
      data: null,
    });
  };
  this.addUser = function (user) {
    return axios({
      url: "https://5f544347e5de110016d51ecc.mockapi.io/users",
      method: "POST",
      data: user,
    });
  };
  this.getDetailUser = function (id) {
    return axios({
      url: "https://5f544347e5de110016d51ecc.mockapi.io/users/" + id,
      method: "GET",
      data: null,
    });
  };
  this.deleteUser = function (id) {
    return axios({
      url: "https://5f544347e5de110016d51ecc.mockapi.io/users/" + id,
      method: "DELETE",
      data: null,
    });
  };
  this.updateUser = function (id, user) {
    return axios({
      url: "https://5f544347e5de110016d51ecc.mockapi.io/users/" + id,
      method: "PUT",
      data: user,
    });
  };
}
