//array 
// var arr = [1,2,3,4,"text",null,undefined];

//syntax
// console.log(arr[5])
//thêm môt phần tử vào cuối mảng
// arr.push(4);
// console.log(arr);
//thêm môt phần tử vào đầu của mảng
// arr.unshift(10);
//hàm xóa cuối
// arr.pop();
//hàm xóa đàu
// arr.shift();
//xóa theo vi trí
//arr.splice(star,"số lượng phân tử bạn muốn xóa");
// arr.splice(5,1);
// console.log(arr);

// vòng lặp 
//for,while

//for 
// var i =3
// for(i;i < 10; i++){
//     console.log("number :",i);
// }


//white
// var i = 0;
// while(i < 10){
//     console.log("number", i);
//     i++;
// }


//bài tập cho1 mảng số => in ra số lẻ trong mảng
function printOdds(arr){
    for(var i = 0;i < arr.length; i++){
        if(arr[i] % 2 === 1){
            console.log(arr[i],"là số lẻ");
        }
    }
    
}

printOdds([0,2,3,4,5,6,7,8,9]);


//2. cho 1 số, kiểm tra số nguyên tô 
function checkPrime(num){
   for(var i = 2; i <  num / 2;i++){
       if(num % i === 0) return false;

   }
   return true;
  
}

checkPrime(5); //true
checkPrime(8);  //false
console.log(checkPrime(5));
console.log(checkPrime(8));
