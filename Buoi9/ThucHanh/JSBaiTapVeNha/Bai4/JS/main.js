// Bài 4: Bài tập tính tiền điện, xây giao diện nhập số KWH điện => số tiền phải trả theo mức giá sau:

// * 50 KWH đầu tiên|: 3000 VND

// * 50 KWH tiếp theo : 2500 VND

// *20 KWH tiếp theo : 2300 VND

// * Còn lại: 2000 VND

function tinhTienDien(){
    var sodien = +document.getElementById("sodien").value;
    var thgP = document.getElementById("tongtien");
    var tongtiendien;
    if(sodien <= 50){
        tongtiendien = sodien * 3000;
        thgP.innerHTML = "Tiền Điên là : " + tongtiendien;
        thgP.style.color ="red";
    }else if(sodien <= 100){
        tongtiendien = (50 * 3000) + ((sodien - 50) * 2500);
        thgP.innerHTML = "Tiền Điên là : " + tongtiendien;
        thgP.style.color ="red";
    }else if(sodien <= 120){
        tongtiendien = (50 * 3000) + (50 * 2500) + ((sodien - 100) * 2300);
        thgP.innerHTML = "Tiền Điên là : " + tongtiendien;
        thgP.style.color ="red";
    }else if(sodien > 120){
        tongtiendien = (50 * 3000) + (50 * 2500) + (20 * 2300) + ((sodien - 120) * 2000);
        thgP.innerHTML = "Tiền Điên là : " + tongtiendien;
        thgP.style.color ="red";
    }else{
        thgP.innerHTML = "Vui lòng nhập KWD điện  ";
        thgP.style.color ="red";
    }
}