//Bài 8:  Viết hàm truyền vào một số n, tính giai thừa n!, với n! = 1*2*3*4*5.....*n
function tinhGiaiThua(n){
    if(n ===1){
        return 1;

    }
    return n * tinhGiaiThua(n-1);
    
}
console.log(tinhGiaiThua(5));