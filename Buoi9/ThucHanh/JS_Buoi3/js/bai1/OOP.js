function hienThiThongTin() {
  //  console.log("sdsdads")
  var _maSV = getEle("txtMaSV").value;
  var _tenSV = getEle("txtTenSV").value;
  var _loaiSV = getEle("loaiSV").value;
  var _diemToan = +getEle("txtDiemToan").value;
  var _diemVan = +getEle("txtDiemVan").value;

  // show data
  //console.log(_maSV,_tenSV,_loaiSV,_diemToan,_diemVan);

  /**tao
   * doi tuong
   *
   */

  var sinhVien = {
    //key:value (thuoctinh)
    maSV: _maSV,
    hoTen: _tenSV,
    loaiSV: _loaiSV,
    diemToan: _diemToan,
    diemVan: _diemVan,

    //phuong thuc
    tinhDTB: function () {
      return (this.diemToan + this.diemVan) / 2;
    },

    xepLoai: function () {
      var xl = this.tinhDTB();
      if (xl > 8) {
        return "gioi";
      } else if (xl > 5) {
        return "kha";
      } else {
        return "yeu";
      }
    },
  };

  //show info
  getEle("spanTenSV").innerHTML = sinhVien.hoTen;
  getEle("spanMaSV").innerHTML = sinhVien.maSV;
  getEle("spanLoaiSV").innerHTML = sinhVien.loaiSV;
  getEle("spanDTB").innerHTML = sinhVien.tinhDTB();
  getEle("spanXepLoai").innerHTML = sinhVien.xepLoai();

  //console.log(sinhVien);
  //console.log(sinhVien.hoTen, sinhVien.maSV);
}

function getEle(id) {
  return document.getElementById(id);
}
