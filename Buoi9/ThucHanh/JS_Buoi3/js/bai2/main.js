//mang danh sach sv
var dssv = new DanhSachSinhVien();

function themSinhVien() {
  var _maSV = getEle("txtMaSV").value;
  var _tenSV = getEle("txtTenSV").value;
  var _loaiSV = getEle("loaiSV").value;
  var _diemToan = +getEle("txtDiemToan").value;
  var _diemLy = +getEle("txtDiemLy").value;
  var _diemHoa = +getEle("txtDiemHoa").value;
  var _diemRenLuyen = +getEle("txtDiemRenLuyen").value;

  //console.log(sinhVien);

  // dooi tuong sinh vien
  //   var sinhVien = {
  //     // thuoc tinh key:value
  //     maSV: _maSV,
  //     tenSV: _tenSV,
  //     loaiSV: _loaiSV,
  //     diemToan: _diemToan,
  //     diemLy: _diemLy,
  //     diemHoa: _diemHoa,
  //     diemTrenLuyen: _diemRenLuyen,

  //     tinhDTB: function () {
  //       return (this.diemToan + this.diemLy + this.diemHoa) / 3;
  //     },
  //   };
  // console.log(sinhVien);

  // khoi tao opject = tu khoa khai bao new
  var sinhVien = new SinhVien(
    _maSV,
    _tenSV,
    _loaiSV,
    _diemToan,
    _diemLy,
    _diemHoa,
    _diemRenLuyen
  );

  dssv.themSinhVien(sinhVien);
  //console.log(dssv.arr);

  //console.log(sinhVien);
  taoBang(dssv.arr);
}

//ham tao bang

function taoBang(arr) {
  var tbody = document.getElementById("tbodySinhVien");
  tbody.innerHTML = "";
  for (var i = 0; i < arr.length; i++) {
    //tao dong
    var tagTR = document.createElement("tr");
    //tap cot
    var tagTDMaSV = document.createElement("td");
    var tagTDTenSV = document.createElement("td");
    var tagTDLoaiSV = document.createElement("td");
    var tagTDDTB = document.createElement("td");
    var tagTDDTL = document.createElement("td");

    //gan value cho td
    tagTDMaSV.innerHTML = arr[i].maSV;
    tagTDTenSV.innerHTML = arr[i].tenSV;
    tagTDLoaiSV.innerHTML = arr[i].loaiSV;
    tagTDDTB.innerHTML = arr[i].tinhDTB();
    tagTDDTL.innerHTML = arr[i].diemTrenLuyen;

    //appen td vao tr
    tagTR.appendChild(tagTDMaSV);
    tagTR.appendChild(tagTDTenSV);
    tagTR.appendChild(tagTDLoaiSV);
    tagTR.appendChild(tagTDDTB);
    tagTR.appendChild(tagTDDTL);

    //apen tr vaof tbody
    tbody.appendChild(tagTR);
  }
}

function getEle(id) {
  return document.getElementById(id);
}
//console.log("sss");
//ten lop doi tuong chu cai dau viet hoa SinhVien
// ten khoi tao doi tuong tuong viet chu cai dau viet thuong sinhVien
