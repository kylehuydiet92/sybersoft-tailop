//js <==> ES5 k co class === function
//day la lop doi tuong
function SinhVien(
  _maSV,
  _tenSV,
  _loaiSV,
  _diemToan,
  _diemLy,
  _diemHoa,
  _diemRenLuyen
) {
    //key = value
  this.maSV = _maSV;
  this.tenSV = _tenSV;
  this.loaiSV = _loaiSV;
  this.diemToan = _diemToan;
  this.diemLy = _diemLy;
  this.diemHoa = _diemHoa;
  this.diemTrenLuyen = _diemRenLuyen;


  this.tinhDTB = function() {
        return (this.diemToan + this.diemLy + this.diemHoa) / 3;
  } 

}
