import logo from "./logo.svg";
import "./App.css";
import Home from "./pages/home";
import SignIn from "./pages/login";
import MovieDetail from "./pages/movie-detail";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Header from "./components/header";
import Booking from "./pages/booking";

function App() {
  return (
    <div className="App">
      {/* url :http://localhost:3000/movie-detail */}
      <BrowserRouter>
        <Header />
        <Switch>
          {/*http://localhost:3000/  */}
          {/* exact = { true} so sanh chinh sac */}
          <Route path="/" exact={true}>
            <Home></Home>
          </Route>
          <Route path="/sign-in" exact={true}>
            <SignIn></SignIn>
          </Route>
          {/* url :http://localhost:3000/movie-detail */}
          <Route path="/movie-detail/:movieCode" exact={true}>
            <MovieDetail />
          </Route>
          <Route path="/booking/:maLichChieu">
            <Booking />
          </Route>

          {/* khi url = "/home" nó chuyển về "/" */}
          <Route path="/home">
            <Redirect to="/"></Redirect>
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
