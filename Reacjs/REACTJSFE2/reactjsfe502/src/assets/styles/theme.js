import { createMuiTheme } from "@material-ui/core";

const theme = createMuiTheme({
  // sữa thêm của material-ui
  palette: {
    primary: {
      light: "#E5101D",
      main: "#e5101d",
      dark: "#BC000B",
    },
    // tao maauf mowi
    newColor: "pink",
  },
});
export default theme;
