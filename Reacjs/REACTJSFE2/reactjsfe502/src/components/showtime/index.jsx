import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
import format from "date-format";
import { useStyle } from "./styles";
import { useHistory } from "react-router-dom";

function ShowTime() {
  //su dung style
  const classes = useStyle();
  const history = useHistory();
  const lichChieu = useSelector((state) => {
    return state.movie?.movieInfo?.lichChieu;
  });
  console.log("lich Chiếu", lichChieu);

  function renderListShowTime() {
    return lichChieu?.map((showTime, index) => {
      return (
        <TableRow key={index}>
          <TableCell>{showTime.thongTinRap.tenHeThongRap}</TableCell>
          <TableCell>{showTime.thongTinRap.tenCumRap}</TableCell>
          <TableCell>
            {format("dd-MM-yyyy", new Date(showTime.ngayChieuGioChieu))}
          </TableCell>
          <TableCell>
            {format("hh:mm", new Date(showTime.ngayChieuGioChieu))}
          </TableCell>
          <TableCell>
            <Button
              className={classes.btnBooking} //de css
              variant="contained"
              color="primary" //xanh => ddor
              onClick={() => {
                //chuyển sang booking
                history.push(`/booking/${showTime.maLichChieu}`);
              }}
            >
              Đặt vé
            </Button>
          </TableCell>
        </TableRow>
      );
    });
  }

  return (
    <div>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Tên Hệ Thống Rạp</TableCell>
              <TableCell>Tên Cụm Rạp</TableCell>
              <TableCell>Ngày chiếu</TableCell>
              <TableCell>Giờ Chiếu</TableCell>
              <TableCell>Đặt vé</TableCell>
            </TableRow>
          </TableHead>

          <TableBody>{renderListShowTime()}</TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
export default ShowTime;
