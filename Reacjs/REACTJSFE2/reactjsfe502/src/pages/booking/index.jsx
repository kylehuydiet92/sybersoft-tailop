import { Button } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getBookingRequest,
  postBookingRequest,
} from "../../redux/actions/booking.action";
import useStyle from "./style";
import { useParams } from "react-router-dom";

export default function Booking() {
  //Hook
  const { maLichChieu } = useParams();
  const classes = useStyle();
  const dispatch = useDispatch();
  const danhSachGhe = useSelector((state) => state.booking.danhSachGhe);
  console.log("danhsachsghe", danhSachGhe);

  //   kiểm tra trang thái ghê
  function trangThaiGhe(daDat, dangChon) {
    if (daDat) {
      // đã đặt
      return classes.daDat;
    } else {
      if (dangChon) {
        return classes.dangChon;
      } else {
        return classes.chuaDat;
      }
    }
  }

  function renderGhe() {
    return danhSachGhe.map((ghe, index) => {
      return (
        <Button
          className={trangThaiGhe(ghe.daDat, ghe.dangChon)}
          key={index}
          onClick={() => {
            dispatch({ type: "CHON_GHE", payload: ghe });
          }}
        >
          {ghe.stt}
        </Button>
      );
    });
  }

  // chỉ chạy 1 lần duy nhất khi commonent đc gọi
  useEffect(function () {
    //   dispastch action để tương tác vs api
    dispatch(getBookingRequest(maLichChieu));
  }, []);

  function handleBooking() {
    let danhSachVe = danhSachGhe.filter((ghe) => ghe.dangChon);
    danhSachVe = danhSachVe.map((ghe) => ({
      maGhe: ghe.maGhe,
      giaVe: ghe.giaVe,
    }));
    dispatch(postBookingRequest(maLichChieu, danhSachVe));
  }
  return (
    <div>
      <h1>BOoking Ghe</h1>
      <div>{renderGhe()}</div>;
      <div>
        <Button
          onClick={() => {
            handleBooking();
          }}
          variant="contained"
          color="secondary"
        >
          Đặt vé
        </Button>
      </div>
    </div>
  );
}
