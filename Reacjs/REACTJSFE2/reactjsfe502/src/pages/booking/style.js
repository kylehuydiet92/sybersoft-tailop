import { makeStyles } from "@material-ui/core";

const useStyle = makeStyles(() => ({
  // key:value
  // chưa đặt
  chuaDat: {
    backgroundColor: "red",
    "&:hover": {
      backgroundColor: "red",
    },
  },
  daDat: {
    backgroundColor: "blue",
    "&:hover": {
      backgroundColor: "blue",
    },
  },
  dangChon: {
    backgroundColor: "yellow",
    "&:hover": {
      backgroundColor: "yellow",
    },
  },
}));

export default useStyle;
