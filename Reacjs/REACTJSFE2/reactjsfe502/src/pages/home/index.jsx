import React, { Component } from "react";
import { getMovieListRequest } from "../../redux/actions/movie.actions";
import { connect } from "react-redux";
import { CircularProgress, LinearProgress, Grid } from "@material-ui/core";
import MovieItem from "../../components/movieItem";

class Home extends Component {
  renderMovie = () => {
    const { movieList } = this.props;
    //c1
    // if (movieList !== null) {
    //   movieList.map((item, index) => {
    //     return (
    //       <Grid item sm={3} key={index}>
    //         <MovieItem item={item} />
    //       </Grid>
    //     );
    //   });
    // }
    //c2:
    return movieList?.map((item, index) => {
      return (
        <Grid item sm={3} key={index}>
          <MovieItem item={item} />
        </Grid>
      );
    });
  };

  render() {
    const { isLoading } = this.props;
    if (isLoading) {
      return <LinearProgress />;
    }
    return (
      <div>
        <h1 className="text-center text-info">Home</h1>
        <Grid container>{this.renderMovie()}</Grid>
      </div>
    );
  }
  componentDidMount() {
    //call api
    this.props.dispatch(getMovieListRequest());
  }
}
const mapStateToProp = (state) => {
  return {
    isLoading: state.common.isLoading,
    movieList: state.movie.movieList,
  };
};
export default connect(mapStateToProp)(Home);
