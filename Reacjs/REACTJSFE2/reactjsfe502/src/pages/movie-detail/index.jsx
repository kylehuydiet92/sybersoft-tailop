import React, { useEffect, useState } from "react";
//useEffect để sử dung livecycer
import { getMovieDetaiRequest } from "../../redux/actions/movie.actions";
import { useDispatch } from "react-redux";
import ShowTime from "../../components/showtime";
import { useParams } from "react-router-dom";

export default function MovieDetail() {
  const [star, setStar] = useState(0);
  const dispatch = useDispatch();

  const { movieCode } = useParams();
  console.log(useParams());
  console.log(movieCode);

  useEffect(function () {
    //chay khi render chạy lại
    //dispatch action , call api , tương tác DOM
    console.log("useEfffect");
    //closurefunction
    return function () {
      //dọn dẹp máy các tác vụ như là clearInterval,clearTimeout
    };
  });

  //chỉ chạy một lần, khi component được gọi
  useEffect(function () {
    //chay khi render chạy lại
    //dispatch action , call api , tương tác DOM
    console.log("useEfffect chạy 1 lần");
    dispatch(getMovieDetaiRequest(movieCode));
    //closurefunction
    return function () {
      //dọn dẹp máy các tác vụ như là clearInterval,clearTimeout
    };
  }, []);

  //chay khi star thay đổi
  useEffect(
    function () {
      //chay khi render chạy lại
      //dispatch action , call api , tương tác DOM
      console.log("useEfffect chay khi star thay đổi");
      //closurefunction
      return function () {
        //dọn dẹp máy các tác vụ như là clearInterval,clearTimeout
      };
    },
    [star]
  );

  return (
    <div>
      <p>{star}</p>
      <button
        onClick={() => {
          const newStar = star + 1;
          setStar(newStar);
        }}
      >
        Tăng sao
      </button>
      \
      <ShowTime />
    </div>
  );
}
