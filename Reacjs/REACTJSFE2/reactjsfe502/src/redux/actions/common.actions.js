export function startLoading() {
  return {
    type: "START__LOADING",
  };
}

export function stopLoading() {
  return {
    type: "STOP__LOADING",
  };
}
