import Axios from "axios";
import { startLoading, stopLoading } from "./common.actions";

export function getMovieListRequest() {
  return (dispatch) => {
    //start loading

    dispatch(startLoading());

    //call api
    Axios.get(
      "https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01"
    )
      .then((res) => {
        console.log(res);
        dispatch(getMovieListSuccess(res.data));
        // dispatch(stoploading)

        setTimeout(() => {
          dispatch(stopLoading());
        }, 500);
      })
      .catch((error) => {
        console.log(error);
        dispatch(getMovieListFailed(error));
        // dispatch(stoploading)
        dispatch(stopLoading());
      });
  };
}

function getMovieListSuccess(movieList) {
  return {
    type: "GET_MOVIE_LIST_SUCCESS",
    payload: movieList,
  };
}

function getMovieListFailed(error) {
  return {
    type: "GET_MOVIE_LIST_FAILED",
    payload: error,
  };
}

export function getMovieDetaiRequest(movieCode) {
  return function (dispatch) {
    //call api
    Axios.get(
      `http://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${movieCode}`
    )
      .then(function (res) {
        console.log(res);
        dispatch(getListMovieDetailSuccess(res.data));
      })
      .catch(function (err) {
        console.log(err);
        dispatch(getListMovieDetailFailed(err));
      });
  };
}
function getListMovieDetailSuccess(movieDetail) {
  return {
    type: "GET__MOVIE__DETAIL__SUCCESS",
    payload: movieDetail,
  };
}
function getListMovieDetailFailed(err) {
  return {
    type: "GET__MOVIE__DETAIL__FAILED",
    payload: err,
  };
}
