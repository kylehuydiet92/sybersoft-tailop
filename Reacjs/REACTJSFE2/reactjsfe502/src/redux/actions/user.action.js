import Axios from "axios";
import { startLoading, stopLoading } from "./common.actions";
export function loginRequest(user, history) {
  return (dispatch) => {
    dispatch(startLoading);
    //cách1
    // Axios({
    //     methot:"POST",
    //     data:{},
    //     url:" đương dẫn api"
    // })
    //call api
    //Cách 2
    Axios.post(
      "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      user
    )
      .then(function (res) {
        console.log(res);
        //lưu xuống local Storage
        localStorage.setItem("user", JSON.stringify(res.data));
        //chuyển về trang trước đó
        // history.goBack();
        //chuyển về home
        history.push("/home");
        dispatch(stopLoading);
      })
      .catch(function (error) {
        //thông báo lỗi cho người dùng
        console.log(error);
        dispatch(stopLoading);
      });
  };
}
