const initialState = {
  isLoading: false,
};

const commonReducer = (state = initialState, action) => {
  let { type, payload } = action; //bóc tach es6
  switch (type) {
    case "START__LOADING": {
      return { ...state, isLoading: true };
    }
    case "STOP__LOADING": {
      return { ...state, isLoading: false };
    }

    default:
      return state;
  }
};
export default commonReducer;
