const initialState = {
  movieList: null,
  movieInfo: null,
};

const movieReducer = (state = initialState, action) => {
  let { type, payload } = action; //bóc tach es6
  switch (type) {
    case "GET_MOVIE_LIST_SUCCESS": {
      //   state.movieList = payload;
      //   return { ...state }; // <=> setState
      return { ...state, movieList: payload };
    }
    case "GET__MOVIE__DETAIL__SUCCESS": {
      return { ...state, movieInfo: payload };
    }
    default:
      return state;
  }
};
export default movieReducer;
