import { createStore, applyMiddleware, compose } from "redux";
//tao rootreducer
import rootreducer from "./reducers/root.reducer";
//khoi tao store
import thunk from "redux-thunk";
//khỏi tạo thunk

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; //khởi tạo để xem redux
const store = createStore(
  rootreducer,
  composeEnhancers(applyMiddleware(thunk))
);

//export dể sài ở index
export default store;
