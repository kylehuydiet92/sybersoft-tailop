import React, { Component } from "react";
import { connect } from "react-redux";
import { deleteAccount } from "../../redux/actions/accountAction";

class AccountItem extends Component {
  render() {
    const { taikhoan, matkhau, loai, id } = this.props.accounts;
    return (
      <tr>
        <td>{taikhoan}</td>
        <td>{matkhau}</td>
        <td>{loai}</td>
        <td>
          <button className="btn btn-warning mr-2">Edit</button>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.deleteAccount(id);
            }}
          >
            Delete
          </button>
        </td>
      </tr>
    );
  }
}
const mapDispathToProps = (dispath) => {
  return {
    deleteAccount: (id) => dispath(deleteAccount(id)),
  };
};
export default connect(null, mapDispathToProps)(AccountItem);
