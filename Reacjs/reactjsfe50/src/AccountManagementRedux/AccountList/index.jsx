import React, { Component } from "react";
import AccountItem from "../AccountItem";
import { connect } from "react-redux";
import { selectAccounts } from "../../redux/selector/accountSelector";

class AccountList extends Component {
  render() {
    return (
      <table className="table table-hover table-dark mt-5">
        <thead>
          <tr>
            <th>Tài khoản</th>
            <th>Mật khẩu</th>

            <th>Loại</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {this.props.accounts.map((item, index) => {
            return <AccountItem accounts={item} key={index} />;
          })}
        </tbody>
      </table>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    accounts: selectAccounts(state),
  };
};
export default connect(mapStateToProps)(AccountList);
