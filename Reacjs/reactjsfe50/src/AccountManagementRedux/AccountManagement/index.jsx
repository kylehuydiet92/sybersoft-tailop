import React, { Component } from "react";
import AccountList from "../AccountList";
import AddUpdateAccount from "../AddUpdateAccont";
import Search from "../Search";

class AccountManagement extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="text-center text-warning"> Account Management</h1>
        <div className="d-flex justify-content-between">
          <Search />

          <button
            type="button"
            className="btn btn-primary btn-lg"
            data-toggle="modal"
            data-target="#modelId"
          >
            Add Account
          </button>
        </div>
        <AccountList />
        <AddUpdateAccount />
      </div>
    );
  }
}

export default AccountManagement;
