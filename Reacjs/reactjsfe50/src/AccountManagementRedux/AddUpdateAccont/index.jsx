import React, { Component } from "react";
import { connect } from "react-redux";
import { saveAccount } from "../../redux/actions/accountAction";

class AddUpdateAccount extends Component {
  state = {
    taikhoan: "",
    matkhau: "",
    loai: "",
    id: "",
  };

  handleChange = (evt) => {
    // console.log(evt.target.value);
    const { value, name } = evt.target;
    this.setState({
      [name]: value,
    });
  };
  handleSubmit = () => {
    this.props.saveAccount(this.state);
  };

  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Modal title</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <input
                  type="text"
                  name="taikhoan"
                  id="taikhoan"
                  className="form-control"
                  placeholder="Tài khoản"
                  aria-describedby="helpId"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="matkhau"
                  id="matkhau"
                  className="form-control"
                  placeholder="Mật Khẩu"
                  aria-describedby="helpId"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="loai"
                  id="loai"
                  className="form-control"
                  placeholder="Loại"
                  aria-describedby="helpId"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => {
                  this.handleSubmit();
                }}
              >
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapDispathToProps = (dispath) => {
  return {
    saveAccount: (account) => dispath(saveAccount(account)),
  };
};
export default connect(null, mapDispathToProps)(AddUpdateAccount);
