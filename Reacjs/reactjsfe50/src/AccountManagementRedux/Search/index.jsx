import React, { Component } from "react";
import { connect } from "react-redux";
import { searchAccount } from "../../redux/actions/accountAction";

class Search extends Component {
  handleSearch = (evt) => {
    // console.log(evt.target);
    this.props.searchAcount(evt.target.value);
  };
  render() {
    return (
      <div>
        <input
          type="text"
          className="form-control"
          name
          id
          aria-describedby="helpId"
          placeholder="Search Account"
          onChange={this.handleSearch}
        />
      </div>
    );
  }
}
const mapDispathToProps = (dispath) => {
  return {
    searchAcount: (keyword) => {
      dispath(searchAccount(keyword));
    },
  };
};

export default connect(null, mapDispathToProps)(Search);
