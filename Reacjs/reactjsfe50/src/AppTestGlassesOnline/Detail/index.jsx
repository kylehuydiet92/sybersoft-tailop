import React, { Component } from "react";
import model from "../../assets/img/model.jpg";
import "./style.css";

class Detail extends Component {
  render() {
    const { url, name, desc } = this.props.selectedProduct;
    return (
      <div
        style={{
          width: "200px",
          height: "250px",
          position: "relative",
          margin: "100px 0",
          zIndex: "2",
        }}
      >
        <img src={model} alt="" className="model" />
        <div className="glass__item">
          <img src={url} alt="vv" />
        </div>
        <div className="glass__content">
          <h6>{name}</h6>
          <p>{desc}</p>
        </div>
      </div>
    );
  }
}

export default Detail;
