import React, { Component } from "react";
import "./style.css";

class GlassItem extends Component {
  render() {
    const { url } = this.props.prod;
    console.log(url);
    return (
      <div>
        <img
          onClick={() => {
            this.props.getProduct(this.props.prod);
          }}
          src={url}
          alt="g1"
          className="product"
        />
      </div>
    );
  }
}

export default GlassItem;
