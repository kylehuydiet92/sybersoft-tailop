import React, { Component } from "react";
import g1 from "../../assets/img/g1.jpg";
import g2 from "../../assets/img/g2.jpg";
import g3 from "../../assets/img/g3.jpg";
import g4 from "../../assets/img/g4.jpg";
import g5 from "../../assets/img/g5.jpg";
import g6 from "../../assets/img/g6.jpg";
import g7 from "../../assets/img/g7.jpg";
import g8 from "../../assets/img/g8.jpg";
import g9 from "../../assets/img/g9.jpg";
import GlassItem from "../GlassItem";

class GlasProduct extends Component {
  renderGlass = () => {
    return this.props.data.map((item, index) => {
      return (
        <GlassItem
          prod={item}
          key={index}
          getProduct={this.props.getProduct}
        ></GlassItem>
      );
    });
  };
  render() {
    // const url = this.props.sp.url;
    return (
      <div
        className=""
        style={{
          width: "60%",
          height: "150px",
          backgroundColor: "#fff",
          zIndex: "2",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {this.renderGlass()}
      </div>
    );
  }
}

export default GlasProduct;
