import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <div style={{ width: "100%" }}>
        <h1 className="text-center text-warning py-5 bg-dark ">
          TRY GLASSES APP ONLINE
        </h1>
      </div>
    );
  }
}

export default Header;
