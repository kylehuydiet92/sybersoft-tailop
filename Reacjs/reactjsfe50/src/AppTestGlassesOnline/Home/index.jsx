import React, { Component } from "react";
import Glass from "../Glass";

class Home extends Component {
  render() {
    return (
      <div
        className=""
        style={{
          // backgroundImage: ` URL(${Bg})`,
          position: "fixed",
          height: "100%",
          width: "100%",
          left: "0",
          top: "0",
          backgroundColor: "rgba(14, 14, 14, 1)",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div
          style={{
            backgroundColor: "rgba(14, 14, 14, .3)",
            width: "100%",
            height: "100%",
            zIndex: "2",
          }}
        ></div>
        <Glass />
      </div>
    );
  }
}

export default Home;
