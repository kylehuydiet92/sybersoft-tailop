import React from "react";
import MyHeader from "./Header";
import Slide from "./Slide";
import Footer from "./Footer";
import ProductList from "./ProductList";

function BaiTapChiaComponent() {
  return (
    <>
      {/* // fragment */}
      <MyHeader />
      <Slide />
      <ProductList />
      <Footer />
    </>
  );
}

export default BaiTapChiaComponent;
