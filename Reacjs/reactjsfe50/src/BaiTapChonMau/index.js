import React, { Component } from "react";

/**
 * @Cacbuocthuchien
 * b1: dàn layout
 * b2:sác dịnh dữ liệu thây đổi và lưu vào state
 * b3:thức thi các action của người dùng
 * b4
 */
export default class BaiTapChonMauXe extends Component {
  state = {
    imgCar: "",
  };

  handleChangeColor = (imgCar) => {
    // immutable = tính bắt biến của dữ liệu
    console.log(imgCar);
    this.setState({
      imgCar,
    });
  };

  render() {
    return (
      <section className="show-room">
        <h2 className="text-center">Bài Tập Chọn Màu Xe</h2>
        <div className="container">
          <div className="chose__color d-flex justify-content-around">
            <a
              href="/"
              onClick={(e) => {
                e.preventDefault();
                this.handleChangeColor("./img/car/products/black-car.jpg");
              }}
            >
              <img
                src="./img/car/icons/icon-black.jpg"
                alt="hinh"
                style={{ width: 50 }}
              />
            </a>
            <a
              href="/"
              onClick={(e) => {
                e.preventDefault();
                this.handleChangeColor("./img/car/products/red-car.jpg");
              }}
            >
              <img
                src="./img/car/icons/icon-red.jpg"
                alt="hinh"
                style={{ width: 50 }}
              />
            </a>
            <a
              href="/"
              onClick={(e) => {
                e.preventDefault();
                this.handleChangeColor("./img/car/products/silver-car.jpg");
              }}
            >
              <img
                src="./img/car/icons/icon-silver.jpg"
                alt="hinh"
                style={{ width: 50 }}
              />
            </a>
            <a
              href="/"
              onClick={(e) => {
                e.preventDefault();
                this.handleChangeColor("./img/car/products/steel-car.jpg");
              }}
            >
              <img
                src="./img/car/icons/icon-steel.jpg"
                alt="hinh"
                style={{ width: 50 }}
              />
            </a>
          </div>
          <div className="car">
            <img className="img-thumbnail" src={this.state.imgCar} alt="hinh" />
          </div>
        </div>
      </section>
    );
  }
}
