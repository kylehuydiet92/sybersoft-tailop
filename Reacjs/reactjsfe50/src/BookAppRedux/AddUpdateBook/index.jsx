import React, { Component } from "react";
import { connect } from "react-redux";
import { saveBook } from "../../redux/actions/bookAction";

class AddUpdateBook extends Component {
  state = {
    name: "",
    price: "",
    type: "",
  };

  handleChange = (evt) => {
    const { value, name } = evt.target;
    this.setState({
      [name]: value,
    });
  };
  handleSubmit = () => {
    this.props.saveBook(this.state);
  };
  //cung cap 2 tham so prevProps vs pevState
  componentDidUpdate(prevProps) {
    console.log("prevProps", prevProps);
    console.log("props hiên tại", this.props.selectedBook);
    console.log("run componentDidUpdate");
    if (prevProps.selectedBook !== this.props.selectedBook) {
      this.setState(this.props.selectedBook);
    }
  }
  render() {
    console.log("run Render");
    return (
      <div
        className="modal fade"
        id="modelBook"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Modal title</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <label htmlFor="name" />
                <input
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                  value={this.state.name}
                  placeholder="Name"
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="price" />
                <input
                  type="text"
                  name="price"
                  id="price"
                  className="form-control"
                  value={this.state.price}
                  placeholder="price"
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="type" />
                <input
                  type="text"
                  name="type"
                  id="type"
                  className="form-control"
                  value={this.state.type}
                  placeholder="type"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => {
                  this.handleSubmit();
                }}
              >
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispathToProps = (dispath) => {
  return {
    saveBook: (book) => dispath(saveBook(book)),
  };
};
const mapStateToProps = (state) => {
  return {
    selectedBook: state.bookReducer.selectedBook,
  };
};
export default connect(mapStateToProps, mapDispathToProps)(AddUpdateBook);
