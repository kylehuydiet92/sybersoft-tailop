import React, { Component } from "react";
import { connect } from "react-redux";
import { deleteBook, selectedBook } from "../../redux/actions/bookAction";

class BookItem extends Component {
  render() {
    const { name, price, type, id } = this.props.book;
    return (
      <tr>
        <td>{name}</td>
        <td>{price}</td>
        <td>{type}</td>
        <td>
          <button
            className="btn btn-primary mr-3"
            data-toggle="modal"
            data-target="#modelBook"
            onClick={() => {
              this.props.selectedBook(this.props.book);
            }}
          >
            Edit
          </button>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.deleteBook(id);
            }}
          >
            Delete
          </button>
        </td>
      </tr>
    );
  }
}
const mapDispathToProps = (dispath) => {
  return {
    deleteBook: (id) => dispath(deleteBook(id)),
    selectedBook: (book) => dispath(selectedBook(book)),
  };
};
export default connect(null, mapDispathToProps)(BookItem);
