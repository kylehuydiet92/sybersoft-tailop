import React, { Component } from "react";
import { connect } from "react-redux";
import BookItem from "../BookItem";
import { selectBooks } from "../../redux/selector/bookSelector";
class BookList extends Component {
  constructor(props) {
    super(props);
    this.setState({
      bookList: [],
    });
  }
  //fillter mãng bookList Trước khi render ra UI
  static getDerivedStateFromProps(props, state) {
    let { books, search } = props;
    books = books.filter((book) => {
      return book.name.indexOf(search) !== -1;
    });
    return {
      bookList: books,
    };
    // cập nhập lại state
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log(nextProps, nextState);
    console.log("shouldComponentUpdate");
    if (nextState.bookList.length > 0) {
      return true;
      //chạy xuoong render
    } else {
      return false;
      //dừng chương trình k xuống render
    }
  }
  render() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th>Tên sách</th>
            <th>Giá</th>
            <th>Loại</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {/* {this.props.books.map((item, index) => {
            return <BookItem book={item} key={index} />;
          })} */}
          {this.state.bookList.map((item, index) => {
            return <BookItem book={item} key={index} />;
          })}
        </tbody>
      </table>
    );
  }
}

const mapStatetoProps = (state) => {
  return {
    // books: selectBooks(state),
    books: state.bookReducer.books,
    search: state.bookReducer.search,
  };
};

export default connect(mapStatetoProps)(BookList);
