import React, { Component } from "react";
import AddUpdateBook from "../AddUpdateBook";
import BookList from "../BookList";
import Search from "../SearchBook";

class Home extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="text-center">Book Store</h1>
        <div className="d-flex justify-content-between my-3">
          <Search />
          <button
            className="btn-warning btn"
            data-toggle="modal"
            data-target="#modelBook"
          >
            Add Book
          </button>
        </div>
        <BookList />
        <AddUpdateBook />
      </div>
    );
  }
}

export default Home;
