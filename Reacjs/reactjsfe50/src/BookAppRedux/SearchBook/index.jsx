import React, { Component } from "react";
import { connect } from "react-redux";
import { searchBooks } from "../../redux/actions/bookAction";
class Search extends Component {
  handleSearch = (evt) => {
    console.log(evt.target.value);
    this.props.searchBooks(evt.target.value);
  };
  render() {
    return (
      <div>
        <input
          className="form-control   "
          type="text"
          placeholder="Search"
          onChange={this.handleSearch}
        />
      </div>
    );
  }
}

const mapDispathToProps = (dispath) => {
  return {
    searchBooks: (keywork) => {
      dispath(searchBooks(keywork));
    },
  };
};

export default connect(null, mapDispathToProps)(Search);
