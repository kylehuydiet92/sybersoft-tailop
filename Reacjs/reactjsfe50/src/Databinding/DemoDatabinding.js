import React, { Component } from "react";

export default class DemoDatabinding extends Component {
  // ten và lớp là thuốc tính cảu class DemoDataBinding
  ten = "Nguyễn Đức Mạnh";
  lop = 50;

//   phương thuc  array function
    renderInfo = () =>{
        return <div>
            <h1>Hôm nay là chủ nhật</h1>
        </div>
    };
  

  render() {
      const hinhAnh = "https://thuthuatnhanh.com/wp-content/uploads/2019/11/hinh-nen-dien-thoai-dep-585x390.jpg";
    return (
      <div>
        <h2>Databinding</h2>
        <p>Tên : {this.ten}</p>
        <p>Lớp : {this.lop}</p>
        <img src={hinhAnh} alt="hinhdep"/>
        {this.renderInfo()}
      </div>
    );
  }
}
