import React, { Component } from "react";

export default class DemoLogin extends Component {
  /**
   * @isLogin là thuộc tính cảu calss
   * true : dã login
   * false : chưa login
   */
  state = {
    isLogin: false,
  };

  isLogin = false;

  handleClick = () => {
    // this.state,isLogin = true; cách này sai
    this.setState({
      isLogin: true,
    });
  };
  renderLogin = () => {
    if (this.state.isLogin) {
      return <p>Mạnh</p>;
    } else {
      return <button onClick={this.handleClick}>Login</button>;
    }
    // toán tử 3 ngôi ( dieu kien ? thuc thi dung : thuc thi dieu kien sai)
    //     return this.isLogin ? (
    //       <p>Mạnh</p>
    //     ) : (
    //       <button onClick={this.handleClick}>Login</button>
    //     );
    //   };
  };
  render() {
    return (
      <div>
        <h2>State and if else</h2>
        {this.renderLogin()}
      </div>
    );
  }
}
