import React, { Component } from "react";

export default class HandleEvenClass extends Component {
  // phương thức
  handleClick = (center) => {
    alert(`Hello Mạnh ${center}`);
  };

  render() {
    return (
      <div>
        <h2>Handle Evenr Class</h2>
        <button
          onClick={() => {
            this.handleClick("K50");
          }}
        >
          Click My
        </button>
      </div>
    );
  }
}
