import React from 'react'

export default function HandleEvenFunction() {

    const handleClick = (ten) =>{
        alert(`Heloo : ${ten}`)
    };
    return (
        <div>
            <h2> Handle Evens Function</h2>
            {/* Bắt sử lý sự kiên trong js */}
            {/* <button onClick="handleClick()">Click me</button> */}
            {/* sử lý sự kiên trong reactjs */}
            <br></br>
            {/* <button onClick={handleClick}>Click me Reactjs</button> */}
            {/* sử lý sự kiên trong reacjs có paramaster */}
            <br/>
            <button onClick={() =>{
                handleClick("mạnhj")
            }}>Click me Reactjs có tham số</button>

        </div>
    )
}
