import React, { Component, createRef } from "react";
import DemoUnmmounting from "./demoUnmmounting";

class LifeCycleComponent extends Component {
  constructor(props) {
    super(props);
    console.log("constructor");
    this.state = {
      age: 0,
    };

    this.myInput = createRef();
  }
  /**
   * @getDerivedStateFromProps : chạy khi trong qua trinh
   * mouting vs updateting
   * thường dùng : tính toán dữ liêu trwuocs khi render ra màn hình
   * vd (chart)
   *
   *
   */
  static getDerivedStateFromProps(props, state) {
    console.log(props, state);
    console.log("getDerivedStateFromProps");
    return null; //khi khong acpaj nhật lại state
    return {
      // key: value,
      //cập nhâp lại state
    };
  }

  render() {
    console.log("render");
    return (
      <div>
        <h2 className="text-center text-info my-5">LifeCycle Component</h2>
        <input
          type="text"
          name=""
          id=""
          ref={this.myInput}
          placeholder="LifeCycle Component"
        />
        <p>Tuổi nè :{this.state.age}</p>
        <button
          onClick={() =>
            this.setState({
              age: (this.state.age += 1),
            })
          }
        >
          Tăng tuôi
        </button>
        {this.state.age % 2 === 0 ? <DemoUnmmounting /> : null}
      </div>
    );
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    //   thường dung đẻ call APi,tương tác dom các kiểu,setlai state
    console.log("compnentDidUpdate");
  }

  componentDidMount() {
    //   thường dùng để call API, tương tác dom, ...
    console.log("componentDidMount");
    console.log(this.myInput.current);
    let domReact = this.myInput.current;
    domReact.focus();
    domReact.style.color = "red";
    domReact.style.border = " 1px solid blue";
  }
}

export default LifeCycleComponent;
