import React, { Component } from "react";

class DemoUnmmounting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 60,
    };
  }
  render() {
    return (
      <div>
        <h3>Đề mô Unmmounting</h3>
        <p className="text-center text-danger">{this.state.counter}</p>
      </div>
    );
  }
  componentDidMount() {
    this.counter = setInterval(() => {
      this.setState({
        counter: (this.state.counter -= 1),
      });
    }, 1000);
  }

  componentWillUnmount() {
    console.log("SDasdsadsdad");
    clearInterval(this.counter);
  }
}

export default DemoUnmmounting;
