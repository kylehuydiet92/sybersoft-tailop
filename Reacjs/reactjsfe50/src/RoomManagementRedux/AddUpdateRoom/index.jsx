import React, { Component } from "react";
import { connect } from "react-redux";
import { saveRoom } from "../../redux/actions/roomAction";

class AddUpdateRoom extends Component {
  state = {
    id: "",
    soPhong: "",
    tenPhong: "",
    loaiPhong: "",
    giaPhong: "",
  };

  handleChange = (evt) => {
    const { value, name } = evt.target;
    console.log(evt.target.value);
    this.setState({
      [name]: value,
    });
  };

  handleSubmit = () => {
    this.props.saveRoom(this.state);
  };
  componentDidUpdate(prevProps) {
    console.log(prevProps);
    if (prevProps.selectedRoom !== this.props.selectedRoom) {
      this.setState(this.props.selectedRoom);
    }
  }
  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Modal title</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <label htmlFor="soPhong" />
                <input
                  type="text"
                  name="soPhong"
                  id="soPhong"
                  className="form-control"
                  placeholder="soPhong"
                  value={this.state.soPhong}
                  aria-describedby="helpId"
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="tenPhong" />
                <input
                  type="text"
                  name="tenPhong"
                  id="tenPhong"
                  className="form-control"
                  placeholder="tenPhong
               "
                  value={this.state.tenPhong}
                  aria-describedby="helpId"
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="loaiPhong" />
                <input
                  type="text"
                  name="loaiPhong"
                  id="loaiPhong"
                  className="form-control"
                  placeholder="loaiPhong
                  "
                  value={this.state.loaiPhong}
                  aria-describedby="helpId"
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="giaPhong" />
                <input
                  type="text"
                  name="giaPhong"
                  id="giaPhong"
                  className="form-control"
                  placeholder="giaPhong
                "
                  value={this.state.giaPhong}
                  aria-describedby="helpId"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button
                type="button"
                className="btn btn-primary"
                onClick={this.handleSubmit}
              >
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapDispathToProps = (dispath) => {
  return {
    saveRoom: (room) => dispath(saveRoom(room)),
  };
};
const mapStateToProps = (state) => {
  return {
    selectedRoom: state.roomReducer.selectedRoom,
  };
};
export default connect(mapStateToProps, mapDispathToProps)(AddUpdateRoom);
