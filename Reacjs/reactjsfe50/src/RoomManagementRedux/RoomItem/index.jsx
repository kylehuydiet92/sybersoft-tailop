import React, { Component } from "react";
import { connect } from "react-redux";
import { deleteRoom, selectedRoom } from "../../redux/actions/roomAction";

class RoomItem extends Component {
  render() {
    const { id, soPhong, tenPhong, loaiPhong, giaPhong } = this.props.room;
    return (
      <tr>
        <td>{soPhong}</td>
        <td>{tenPhong}</td>
        <td>{loaiPhong}</td>
        <td>{giaPhong}</td>
        <td>
          <button
            className="btn btn-warning mr-2"
            data-toggle="modal"
            data-target="#modelId"
            onClick={() => {
              this.props.selectedRoom({
                id,
                soPhong,
                tenPhong,
                loaiPhong,
                giaPhong,
              });
            }}
          >
            Edit
          </button>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.deleteRoom(id);
            }}
          >
            Delete
          </button>
        </td>
      </tr>
    );
  }
}
const mapDispathToProps = (dispath) => {
  return {
    deleteRoom: (id) => dispath(deleteRoom(id)),
    selectedRoom: (room) => dispath(selectedRoom(room)),
  };
};
export default connect(null, mapDispathToProps)(RoomItem);
