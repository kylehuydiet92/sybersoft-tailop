import React, { Component } from "react";
import RoomItem from "../RoomItem";
import { connect } from "react-redux";
import { selectRooms } from "../../redux/selector/roomSelector";

class RoomList extends Component {
  render() {
    return (
      <table className="table mt-4">
        <thead>
          <tr>
            <th>Số Phòng</th>
            <th>Tên Phòng</th>
            <th>Loại Phòng</th>
            <th>Giá</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {this.props.rooms.map((item, index) => {
            return <RoomItem room={item} key={index} />;
          })}
        </tbody>
      </table>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    rooms: selectRooms(state),
  };
};

export default connect(mapStateToProps)(RoomList);
