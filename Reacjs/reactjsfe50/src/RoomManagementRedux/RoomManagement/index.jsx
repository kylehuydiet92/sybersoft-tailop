import React, { Component } from "react";
import RoomList from "../RoomList";
import SearchRoom from "../SearchRoom";
import AddUpdateRoom from "../AddUpdateRoom";

class RoomManagement extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="text-center text-info my-4">Room Management</h1>
        <div className="d-flex justify-content-between my-2">
          <SearchRoom />
          <div>
            <button
              className="btn btn-info"
              data-toggle="modal"
              data-target="#modelId"
            >
              Add Room
            </button>
          </div>
        </div>
        <RoomList />
        <AddUpdateRoom />
      </div>
    );
  }
}

export default RoomManagement;
