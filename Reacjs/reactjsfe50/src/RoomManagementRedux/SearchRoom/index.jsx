import React, { Component } from "react";
import { connect } from "react-redux";
import { searchRoom } from "../../redux/actions/roomAction";

class SearchRoom extends Component {
  handleChange = (evt) => {
    console.log(evt.target.value);
    this.props.searchRoom(evt.target.value);
  };
  render() {
    return (
      <div>
        <input
          type="text"
          className="form-control"
          name
          id
          aria-describedby="helpId"
          placeholder="Search Room"
          onChange={this.handleChange}
        />
      </div>
    );
  }
}
const mapDispathToProps = (dispath) => {
  return {
    searchRoom: (keyword) => {
      dispath(searchRoom(keyword));
    },
  };
};
export default connect(null, mapDispathToProps)(SearchRoom);
