import React, { Component } from "react";

class CartItem extends Component {
  render() {
    let { cart } = this.props;
    return (
      <tr>
        <td>{cart.id}</td>
        <td>{cart.name}</td>
        <td>
          <img src={cart.img} alt="" width="50px" />
        </td>
        <td>{cart.price} VNĐ</td>
        <td>
          <div
            className="btn btn-warning mr-1"
            onClick={() => {
              this.props.handleChangeAmount(cart, false);
            }}
          >
            -
          </div>
          {cart.amount}
          <div
            className="btn btn-warning ml-1"
            onClick={() => {
              this.props.handleChangeAmount(cart, true);
            }}
          >
            +
          </div>
        </td>
        <td>{parseInt(cart.price) * cart.amount} VNĐ</td>
        <td>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.handleDeleteCart(cart);
            }}
          >
            Delete
          </button>
        </td>
      </tr>
    );
  }
}

export default CartItem;
