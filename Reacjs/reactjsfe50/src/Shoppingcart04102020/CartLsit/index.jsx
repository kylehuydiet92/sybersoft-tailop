import React, { Component } from "react";
import CartItem from "../CartItem";

class CartList extends Component {
  renderCartListUI = () => {
    return this.props.cartList.map((cart, index) => {
      return (
        <CartItem
          key={index}
          cart={cart}
          handleChangeAmount={this.props.handleChangeAmount}
          handleDeleteCart={this.props.handleDeleteCart}
        />
      );
    });
  };
  render() {
    return (
      <div>
        {/* Modal */}
        <div
          className="modal fade "
          id="modelId"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div
            style={{
              maxWidth: "1000px",
            }}
            className="modal-dialog"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Modal title</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <table className="table table-dark table-hover">
                  <thead>
                    <tr>
                      <th>Mã sản phẩm</th>
                      <th>Tên sản phẩm</th>
                      <th>Hình ảnh</th>
                      <th>Giá</th>
                      <th>Số Lượng</th>
                      <th>Tổng tiền</th>
                      <th>Hành động</th>
                    </tr>
                  </thead>
                  <tbody>{this.renderCartListUI()}</tbody>
                </table>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Checkout
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CartList;
