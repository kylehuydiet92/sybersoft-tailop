import React, { Component } from "react";
import Detail from "../Detail";
import Header from "../Header";
import ProductList from "../ProductList";
import backgroud from "../../assets/img/bg.png";
import CartList from "../CartLsit";

class Home extends Component {
  data = [
    {
      id: "sp_1",
      name: "iphoneX",
      price: "30000000",
      screen: "screen_1",
      backCamera: "backCamera_1",
      frontCamera: "frontCamera_1",
      img:
        "https://sudospaces.com/mobilecity-vn/images/2019/12/iphonex-black.jpg",
      desc:
        "iPhone X features a new all-screen design. Face ID, which makes your face your password",
    },
    {
      id: "sp_2",
      name: "Note 7",
      price: "20000000",
      screen: "screen_2",
      backCamera: "backCamera_2",
      frontCamera: "frontCamera_2",
      img:
        "https://www.xtmobile.vn/vnt_upload/product/01_2018/thumbs/600_note_7_blue_600x600.png",
      desc:
        "The Galaxy Note7 comes with a perfectly symmetrical design for good reason",
    },
    {
      id: "sp_3",
      name: "Vivo",
      price: "10000000",
      screen: "screen_3",
      backCamera: "backCamera_3",
      frontCamera: "frontCamera_3",
      img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg",
      desc:
        "A young global smartphone brand focusing on introducing perfect sound quality",
    },
    {
      id: "sp_4",
      name: "Blacberry",
      price: "15000000",
      screen: "screen_4",
      backCamera: "backCamera_4",
      frontCamera: "frontCamera_4",
      img:
        "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg",
      desc:
        "BlackBerry is a line of smartphones, tablets, and services originally designed",
    },
  ];
  state = {
    selectedProduct: null,
    cartList: [],
  };

  getProduct = (product) => {
    console.log(product);
    this.setState(
      {
        selectedProduct: product,
      },
      () => {
        console.log(this.state.selectedProduct);
      }
    );
  };

  handleCartList = (product) => {
    // let cartList = this.state.cartList;
    let { cartList } = this.state;
    // findindex là pthwc cua es6 cung cap
    /**
     *  tìm thấy trả về index
     * k tim thây ra về -1
     */
    const index = cartList.findIndex((cart) => {
      return cart.id === product.id;
    });
    // console.log("index:", index);
    if (index !== -1) {
      //tăng số lương
      cartList[index].amount += 1;
    } else {
      //thêm thuộc tính só lượng cho no rồi push vào
      product.amount = 1;
      cartList.push(product);
    }

    this.setState(
      {
        cartList,
      },
      () => {
        console.log(this.state.cartList);
      }
    );
  };

  /// xóa sanrp hẩm
  handleDeleteCart = (cart) => {
    // const index = cart.findIndex((text) => {
    //   return text.id === cart.id;
    // });

    // cart.splice(index, 1);
    let { cartList } = this.state;
    cartList = cartList.filter((item) => {
      return item.id !== cart.id;
    });
    this.setState({
      cartList,
    });
  };

  handleChangeAmount = (cart, status) => {
    console.log(cart, status);
    let { cartList } = this.state;
    const index = cartList.findIndex((item) => {
      return item.id === cart.id;
    });

    if (index !== -1) {
      if (status) {
        // ccoongj
        cartList[index].amount += 1;
      } else {
        //trừ
        if (cartList[index].amount > 1) {
          cartList[index].amount -= 1;
        }
      }
    }

    this.setState({
      cartList,
    });
  };
  //checkout
  // handleChekout = (cart) => {
  //   console.log(cart);
  // };

  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${backgroud})`,
          minHeight: "100vh",
        }}
      >
        <Header />
        <ProductList
          handleCartList={this.handleCartList}
          getProduct={this.getProduct}
          data={this.data}
        />
        {/* {this.state.selectedProduct ? (
          <Detail selectedProduct={this.state.selectedProduct} />
        ) : null} */}

        {this.state.selectedProduct && (
          <Detail selectedProduct={this.state.selectedProduct} />
        )}
        <CartList
          // handleChekout={this.handleChekout}
          handleChangeAmount={this.handleChangeAmount}
          handleDeleteCart={this.handleDeleteCart}
          cartList={this.state.cartList}
        />
      </div>
    );
  }
}

export default Home;
