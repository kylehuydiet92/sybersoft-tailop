import React, { Component } from "react";

class ProductItem extends Component {
  render() {
    const { img, name } = this.props.prod;
    return (
      <div className="card">
        <img
          style={{
            height: "300px",
          }}
          src={img}
          alt="product item"
        />
        <p className="lead text-center">{name}</p>
        <div
          onClick={() => {
            this.props.getProduct(this.props.prod);
          }}
          className="btn btn-success"
        >
          Xem Chi Tiết
        </div>
        <button
          onClick={() => {
            this.props.handleCartList(this.props.prod);
          }}
          className="btn btn-success my-1"
        >
          Thêm vào giỏ hàng
        </button>
      </div>
    );
  }
}

export default ProductItem;
