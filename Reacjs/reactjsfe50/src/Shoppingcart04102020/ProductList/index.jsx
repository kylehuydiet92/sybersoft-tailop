import React, { Component } from "react";
import ProductItem from "../ProductItem";

class ProductList extends Component {
  renderProdcut = () => {
    // const productUIs = [];
    // for (let i = 0 of this.props.data) {
    //   productUIs.push(
    //     <div className="col-3" key={i}>
    //       <ProductItem />
    //     </div>
    //   );
    // }
    const productUIs = this.props.data.map((item, index) => {
      return (
        <div className="col-3" key={index}>
          <ProductItem
            prod={item}
            getProduct={this.props.getProduct}
            handleCartList={this.props.handleCartList}
          />
        </div>
      );
    });
    return productUIs;
  };
  render() {
    return (
      <div className="container mt-5">
        <div className="row">{this.renderProdcut()}</div>
      </div>
    );
  }
}

export default ProductList;
