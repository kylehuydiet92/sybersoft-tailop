import React, { Component } from "react";
import { connect } from "react-redux";
import { actDeleteCart } from "../../redux/actions/gioHangActions";
class CartItem extends Component {
  render() {
    let { cart } = this.props;
    return (
      <tr>
        <td>{cart.id}</td>
        <td>{cart.name}</td>
        <td>
          <img src={cart.img} alt="" width="50px" />
        </td>
        <td>{cart.price} VNĐ</td>
        <td>
          <div
            className="btn btn-warning mr-1"
            onClick={() => {
              this.props.changeAmount(cart, false);
            }}
          >
            -
          </div>
          {cart.amount}
          <div
            className="btn btn-warning ml-1"
            onClick={() => {
              this.props.changeAmount(cart, true);
            }}
          >
            +
          </div>
        </td>
        <td>{parseInt(cart.price) * cart.amount} VNĐ</td>
        <td>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.deleteCart(cart);
            }}
          >
            Delete
          </button>
        </td>
      </tr>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteCart: (cart) => {
      // const actions = {
      //   type: "DELETE_CART_ITEM",
      //   payload: cart,
      // };
      // dispatch(actions);
      dispatch(actDeleteCart(cart));
    },
    changeAmount: (cart, status) => {
      const actions = {
        type: "CHANGE_AMOUNT_CART",

        payload: { cart, status },
      };
      dispatch(actions);
    },
  };
};

export default connect(null, mapDispatchToProps)(CartItem);
