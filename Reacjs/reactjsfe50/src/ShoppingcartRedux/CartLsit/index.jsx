import React, { Component } from "react";
import CartItem from "../CartItem";
import { connect, Connect } from "react-redux";

class CartList extends Component {
  renderCartListUI = () => {
    return this.props.cartList.map((cart, index) => {
      return <CartItem key={index} cart={{ ...cart }} />;
    });
  };
  render() {
    return (
      <div>
        {/* Modal */}
        <div
          className="modal fade "
          id="modelId"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div
            style={{
              maxWidth: "1000px",
            }}
            className="modal-dialog"
            role="document"
          >
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Modal title</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <table className="table table-dark table-hover">
                  <thead>
                    <tr>
                      <th>Mã sản phẩm</th>
                      <th>Tên sản phẩm</th>
                      <th>Hình ảnh</th>
                      <th>Giá</th>
                      <th>Số Lượng</th>
                      <th>Tổng tiền</th>
                      <th>Hành động</th>
                    </tr>
                  </thead>
                  <tbody>{this.renderCartListUI()}</tbody>
                </table>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" className="btn btn-primary">
                  Checkout
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapToStateToProps = (state) => {
  return {
    cartList: state.gioHangReducers.cartList,
  };
};

export default connect(mapToStateToProps)(CartList);
