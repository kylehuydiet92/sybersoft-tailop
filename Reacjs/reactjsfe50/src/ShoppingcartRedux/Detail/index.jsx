import React, { Component } from "react";
import "./style.css";
import { connect } from "react-redux";

class Detail extends Component {
  render() {
    if (!this.props.selectedProduct) {
      return (
        <div>
          <h2 className="text-center text-warning mt-5">Chưa chon sản phẩm</h2>
        </div>
      );
    }
    const {
      name,
      screen,
      price,
      backCamera,
      frontCamera,
      img,
    } = this.props.selectedProduct;
    return (
      <div
        style={{
          backgroundColor: "white",
          borderRadius: "5px",
        }}
        className="container mt-5"
      >
        <div className="row">
          <div className="col-4">
            <p
              style={{
                color: "red",
                backgroundColor: "yellow",
              }}
              className="product-tittle"
            >
              {name}
            </p>
            <img style={{ width: "100%" }} src={img} alt="Product Detail" />
          </div>
          <div className="col-8">
            <h1>Thông Số kỷ thuật</h1>
            <table className="table">
              <tbody>
                <tr>
                  <td>Màn hình</td>
                  <td>{screen}</td>
                </tr>
                <tr>
                  <td>Camera Trước</td>
                  <td>{backCamera}</td>
                </tr>
                <tr>
                  <td>Camera Sau</td>
                  <td>{frontCamera}</td>
                </tr>
                <tr>
                  <td>Giá</td>
                  <td>{price}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    //key:value
    // key là props của components : value của state trên store
    selectedProduct: state.gioHangReducers.selectedProduct,
  };
};

export default connect(mapStateToProps)(Detail);
