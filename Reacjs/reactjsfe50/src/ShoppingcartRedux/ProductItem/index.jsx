import React, { Component } from "react";
import { connect } from "react-redux";

class ProductItem extends Component {
  render() {
    const { img, name } = this.props.prod;
    return (
      <div className="card">
        <img
          style={{
            height: "300px",
          }}
          src={img}
          alt="product item"
        />
        <p className="lead text-center">{name}</p>
        <div
          onClick={() => {
            this.props.getProduct(this.props.prod);
          }}
          className="btn btn-success"
        >
          Xem Chi Tiết
        </div>
        <button
          onClick={() => {
            this.props.addToCard(this.props.prod);
          }}
          className="btn btn-success my-1"
        >
          Thêm vào giỏ hàng
        </button>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    //ket:value
    //key là props của commopent :Value pương thức gửi đa ta lên store
    getProduct: (product) => {
      const actions = {
        type: "GET_PRODUCT",
        payload: product,
      };
      dispatch(actions);
    },
    addToCard: (product) => {
      const action = {
        type: "ADD_TO_CART",
        payload: product,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(ProductItem);
