import React, { Component } from "react";
import ProductItem from "../ProductItem";
// kết nối store
import { connect } from "react-redux";
class ProductList extends Component {
  renderProdcut = () => {
    // const productUIs = [];
    // for (let i = 0 of this.props.data) {
    //   productUIs.push(
    //     <div className="col-3" key={i}>
    //       <ProductItem />
    //     </div>
    //   );
    // }
    const productUIs = this.props.productList.map((item, index) => {
      return (
        <div className="col-3" key={index}>
          <ProductItem
            prod={item}
            // getProduct={this.props.getProduct}
            // handleCartList={this.props.handleCartList}
          />
        </div>
      );
    });
    return productUIs;
  };
  render() {
    return (
      <div className="container mt-5">
        <div className="row">{this.renderProdcut()}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    //key:value
    // key là props của components : value của state trên store
    productList: state.gioHangReducers.productList,
  };
};
export default connect(mapStateToProps)(ProductList);
