import React, { Component } from "react";

class Detail extends Component {
  render() {
    const {
      name,
      screen,
      price,
      backCamera,
      frontCamera,
      img,
    } = this.props.selectProduct;
    return (
      <div
        style={{
          backgroundColor: "white",
          borderRadius: "5px",
        }}
        className="container my-5"
      >
        <div className="row">
          <div className="col-4">
            <p
              style={{
                color: "red",
                backgroundColor: "yellow",
                fontSize: "24px",
                textTransform: "uppercase",
              }}
              className="product-tittle text-center"
            >
              {name}
            </p>
            <img style={{ width: "100%" }} src={img} alt="Product Detail" />
          </div>
          <div className="col-8">
            <h1>Thông Số kỷ thuật</h1>
            <table className="table">
              <tbody>
                <tr>
                  <td>Màn hình</td>
                  <td>{screen}</td>
                </tr>
                <tr>
                  <td>Camera Trước</td>
                  <td>{frontCamera}</td>
                </tr>
                <tr>
                  <td>Camera Sau</td>
                  <td>{backCamera}</td>
                </tr>
                <tr>
                  <td>Giá</td>
                  <td>{price}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default Detail;
