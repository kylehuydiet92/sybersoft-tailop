import React, { Component } from "react";

class ProductItem extends Component {
  render() {
    const { img, name, desc } = this.props.prod;
    return (
      <div className="card">
        <img
          style={{ height: "250px" }}
          className="img-fluid"
          src={img}
          alt="ádsa"
        />
        <div className="cart-body">
          <h1>{name}</h1>
          <p>{desc}</p>
          <div
            onClick={() => {
              this.props.getProduct(this.props.prod);
            }}
            style={{ width: "100%" }}
            className="btn btn-success "
          >
            Xem Chi Tiết
          </div>
        </div>
      </div>
    );
  }
}

export default ProductItem;
