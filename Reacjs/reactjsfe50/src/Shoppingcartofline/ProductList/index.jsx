import React, { Component } from "react";
import ProductItem from "../ProductItem";

class ProductList extends Component {
  renderProduct = () => {
    return this.props.data.map((item, index) => {
      return (
        <div className="col-3" key={index}>
          <ProductItem getProduct={this.props.getProduct} prod={item} />
        </div>
      );
    });
  };

  render() {
    return (
      <div className="container">
        <div className="row mt-5">{this.renderProduct()}</div>
      </div>
    );
  }
}

export default ProductList;
