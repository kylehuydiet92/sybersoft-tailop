import React, { Component } from "react";
import { connect } from "react-redux";
import { saveUser } from "../../redux/actions/userAction";

class AddUpdateUser extends Component {
  state = {
    name: "",
    username: "",
    email: "",
    phoneNumber: "",
    type: "",
  };
  handleChange = (evt) => {
    const { value, name } = evt.target;
    this.setState({
      [name]: value,
    });
  };

  handleSubmit = () => {
    this.props.saveUser(this.state);
  };

  componentDidUpdate(prevProps) {
    console.log("prevProps", prevProps);
    if (prevProps.selectedUser !== this.props.selectedUser) {
      this.setState(this.props.selectedUser);
    }
  }

  render() {
    console.log(this.state);
    return (
      <div>
        {/* Modal */}
        <div
          className="modal fade"
          id="usersModal"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Modal title</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="form-group">
                  <label htmlFor="name">Name</label>
                  <input
                    type="text"
                    id="name"
                    className="form-control"
                    placeholder="name"
                    name="name" //quan trọng
                    value={this.state.name} //quan trọng
                    onChange={this.handleChange}
                    // onChange={(evt) => {
                    //   this.setState({ name: evt.target.value });
                    // }}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="username">username</label>
                  <input
                    type="text"
                    id="username"
                    className="form-control"
                    placeholder="username"
                    name="username" //quan trọng
                    value={this.state.username} //quan trọng
                    onChange={this.handleChange}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="Email">Email</label>
                  <input
                    type="text"
                    id="email"
                    className="form-control"
                    placeholder="Email"
                    name="email" //quan trọng
                    value={this.state.email} //quan trọng
                    onChange={this.handleChange}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="phone">phoneNumber</label>
                  <input
                    type="text"
                    id="phonenumber"
                    className="form-control"
                    placeholder="Phone Number"
                    name="phoneNumber" //quan trọng
                    value={this.state.phoneNumber} //quan trọng
                    onChange={this.handleChange}
                  />
                </div>

                <div className="form-group">
                  <label htmlFor="type">Tyoe</label>
                  <select
                    className="form-control"
                    id="type"
                    name="type" //quan trọng
                    value={this.state.type} //quan trọng
                    onChange={this.handleChange}
                  >
                    <option value="">---</option>
                    <option value="USER">USER</option>
                    <option value="VIP">VIP</option>
                  </select>
                </div>
              </div>

              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => {
                    this.handleSubmit();
                  }}
                >
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispathToProps = (dispath) => {
  return {
    saveUser: (user) => dispath(saveUser(user)),
  };
};
const mapStateToProps = (state) => {
  return {
    selectedUser: state.userReducer.selectedUser,
  };
};

export default connect(mapStateToProps, mapDispathToProps)(AddUpdateUser);
