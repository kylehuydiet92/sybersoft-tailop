import React, { Component } from "react";
import { connect } from "react-redux";
import { searchUsers } from "../../redux/actions/userAction";

class Search extends Component {
  handleSearch = (evt) => {
    //   lấy giá trị ô input
    console.log(evt.target.value);
    this.props.searchUsers(evt.target.value);
    //dispatch action search
  };
  render() {
    return (
      <input
        className="form-control w-50"
        type="text"
        name=""
        id=""
        placeholder="Tìm kiếm"
        onChange={this.handleSearch}
      />
    );
  }
}

const mapDispathToProps = (dispath) => {
  return {
    searchUsers: (keyword) => {
      dispath(searchUsers(keyword));
    },
  };
};
export default connect(null, mapDispathToProps)(Search);
