import React, { Component } from "react";
import { connect } from "react-redux";
import { deleteUser, selectedUser } from "../../redux/actions/userAction";

class UserItem extends Component {
  render() {
    const { name, username, email, phoneNumber, type, id } = this.props.users;
    return (
      <tr>
        <td>{name}</td>
        <td>{username}</td>
        <td>{email}</td>
        <td>{phoneNumber}</td>
        <td>{type}</td>
        <td>
          <button
            className="btn btn-warning mr-1"
            data-toggle="modal"
            data-target="#usersModal"
            onClick={() => {
              this.props.selectedUser({
                name,
                username,
                email,
                phoneNumber,
                type,
                id,
              });
            }}
          >
            Edit
          </button>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.deleteUser(id);
            }}
          >
            Delete
          </button>
        </td>
      </tr>
    );
  }
}

const mapDispathToProps = (dispath) => {
  return {
    deleteUser: (id) => dispath(deleteUser(id)),
    selectedUser: (user) => dispath(selectedUser(user)),
  };
};

export default connect(null, mapDispathToProps)(UserItem);
