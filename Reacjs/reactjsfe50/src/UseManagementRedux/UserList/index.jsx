import React, { Component } from "react";
import { connect } from "react-redux";
import UserItem from "../UserItem";
import { selectUsers } from "../../redux/selector/userSelectors";
class UserList extends Component {
  render() {
    return (
      <div>
        <table className="table table-dark table-hover mt-4 ">
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Email</th>
              <th>PhoneNumber</th>
              <th>Type</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.props.users.map((users, index) => (
              // mở ngoặc tròn là return luôn
              // nguoacwj ngon là phải return
              <UserItem users={users} key={index} />
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToPops = (state) => {
  return {
    users: selectUsers(state),
  };
};

export default connect(mapStateToPops)(UserList);
// kết nối redux lấy user hiển thị ra
