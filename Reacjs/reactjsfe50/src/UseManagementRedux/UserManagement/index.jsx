import React, { Component } from "react";
import Search from "../Search";
import UserList from "../UserList";
import AddUpdateUser from "../AddUpdateUser/";
import bg from "../../assets/img/background.jpg";

class UserManagement extends Component {
  render() {
    return (
      <div
        className="container"
        style={{
          //   background: "pink",
          minHeight: "100vh",
        }}
      >
        <h1 className="text-center text-danger mb-5">User Management</h1>
        <div className="d-flex justify-content-between">
          <Search />
          {/* Button trigger modal */}

          <button
            type="button"
            data-toggle="modal"
            data-target="#usersModal"
            className="btn btn-success"
          >
            Add User
          </button>
        </div>
        <UserList />
        <AddUpdateUser />
      </div>
    );
  }
}

export default UserManagement;
