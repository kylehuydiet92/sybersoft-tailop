import React, { Component } from "react";

class MovieItem extends Component {
  state = {
    isShowDetail: false,
  };

  showDetail() {
    if (this.state.isShowDetail) {
      return this.props.item.moTa;
    } else {
      return this.props.item.moTa.substr(0, 100) + "...";
    }
  }

  onViewDetailClick = () => {
    //immutable tính bắt biến của dữ liệu
    this.setState({
      isShowDetail: !this.state.isShowDetail,
    });
  };

  viewDetailText = () => {
    if (this.state.isShowDetail) return "Rút Gọn";
    else return "Xem Thêm";
  };

  render() {
    //destructring : bóc tách phàn tử
    const { hinhAnh, tenPhim, moTa } = this.props.item;
    // nếu có 2 đói tượng trùng tên
    // const { hinhAnh : img, tenPhim: name, moTa : title } = this.props.item2;
    return (
      <div className="card">
        <img src={hinhAnh} alt="movie" />
        <div className="card-body">
          <h4>{tenPhim}</h4>
          <p>{this.showDetail()}</p>
          <span className="btn btn-info" onClick={this.onViewDetailClick}>
            {/* {this.viewDetailText()} */}
            {this.state.isShowDetail ? "Rút gọn" : "Xem thêm"}
          </span>
        </div>
      </div>
    );
  }
}

export default MovieItem;
