import { DELETE, SELECT, SEARCH, SAVE } from "../contants/accountContant";

export const searchAccount = (keyword) => {
  return {
    type: SEARCH,
    payload: keyword,
  };
};
export const saveAccount = (account) => {
  return {
    type: SAVE,
    payload: account,
  };
};
export const deleteAccount = (id) => {
  return {
    type: DELETE,
    payload: id,
  };
};
