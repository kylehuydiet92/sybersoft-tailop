import { SEARCH, SAVE, DELETE, SELECT } from "../contants/bookContants";

export const searchBooks = (keyword) => {
  return {
    type: SEARCH,
    payload: keyword,
  };
};
export const saveBook = (book) => {
  return {
    type: SAVE,
    payload: book,
  };
};
export const deleteBook = (id) => {
  return {
    type: DELETE,
    payload: id,
  };
};
export const selectedBook = (book) => {
  return {
    type: SELECT,
    payload: book,
  };
};
