import { DELETE_CART_ITEM } from "../contants/gioHangContants";
export function actDeleteCart(cart) {
  return {
    type: DELETE_CART_ITEM,
    payload: cart,
  };
}
