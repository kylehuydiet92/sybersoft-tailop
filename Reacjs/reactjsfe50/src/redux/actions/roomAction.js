import { SELECT, SEARCH, SAVE, DELETE } from "../contants/roomContant";
export const searchRoom = (keyword) => {
  return {
    type: SEARCH,
    payload: keyword,
  };
};
export const saveRoom = (room) => {
  return {
    type: SAVE,
    payload: room,
  };
};
export const deleteRoom = (id) => {
  return {
    type: DELETE,
    payload: id,
  };
};
export const selectedRoom = (room) => {
  return {
    type: SELECT,
    payload: room,
  };
};
