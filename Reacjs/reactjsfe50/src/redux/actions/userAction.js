import { SEARCH, SELECT, SAVE, DELETE } from "../contants/userContants";

export const searchUsers = (keyword) => {
  return {
    type: SEARCH,
    payload: keyword,
  };
};
export const saveUser = (user) => {
  return {
    type: SAVE,
    payload: user,
  };
};
export const deleteUser = (id) => {
  return {
    type: DELETE,
    payload: id,
  };
};
export const selectedUser = (user) => {
  return {
    type: SELECT,
    payload: user,
  };
};
