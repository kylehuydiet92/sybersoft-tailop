const PREFIX = "USERS";

export const SEARCH = `${PREFIX}__SEARCH`;
export const SAVE = `${PREFIX}__SAVE`;
export const SELECT = `${PREFIX}__SELECT`;
export const DELETE = `${PREFIX}__DELETE`;
