import userEvent from "@testing-library/user-event";
import { DELETE, SAVE, SEARCH, SELECT } from "../contants/accountContant";
const initialState = {
  accounts: [
    {
      taikhoan: "kylehuydiet1",
      matkhau: "manh123",
      loai: "Vip",
      id: "1",
    },
    {
      taikhoan: "hoamibay2",
      matkhau: "thaikhong",
      loai: "Vip",
      id: "2",
    },
    {
      taikhoan: "concoco3",
      matkhau: "bao123",
      loai: "Thường",
      id: "3",
    },
  ],
  search: "",
};

const accountReducer = (state = initialState, actions) => {
  switch (actions.type) {
    case SEARCH: {
      return { ...state, search: actions.payload };
    }
    case SAVE: {
      const account = actions.payload;
      if (account.id) {
        const accounts = state.accounts.map((item) => {
          if (account.id === accounts.id) {
            return account;
          }
          return item;
        });
        return { ...state, accounts };
      }

      const id = Math.floor(Math.random() * 100);
      const accounts = [...state.accounts, { ...account, id }];
      return { ...state, accounts };
    }
    case DELETE: {
      const accounts = state.accounts.filter(
        (item) => item.id !== actions.payload
      );
      return { ...state, accounts };
    }
    default:
      return state;
  }
};
export default accountReducer;
