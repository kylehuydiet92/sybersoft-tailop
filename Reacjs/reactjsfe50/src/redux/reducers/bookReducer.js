import { DELETE, SEARCH, SELECT, SAVE } from "../contants/bookContants";
const initialState = {
  books: [
    {
      id: 1,
      name: "Sách hay",
      price: 15000,
      type: "truyên ngắn",
    },
    {
      id: 2,
      name: "Lời hứa ",
      price: 1600,
      type: "truyên dài",
    },
    {
      id: 3,
      name: "Nước mắt",
      price: 1600,
      type: "truyên dài",
    },
  ],
  search: "",
  selectedBook: null,
  // selectedBook: {
  //   id: "",
  //   name: "",
  //   price: "",
  //   type: ""
  // }
};
const bookReducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH: {
      return { ...state, search: action.payload };
    }

    case SAVE: {
      const book = action.payload;
      if (book.id) {
        // const books = state.books.map((item) => {
        //   if (books.id === book.id) {
        //     return book;
        //   }
        //   return item;
        // });

        let books = [...state.books];
        const index = books.findIndex((item) => {
          return book.id === item.id;
        });
        books[index] = book;

        return { ...state, books };
      }
      //nếu k nhây vào if => book chưa tồn tại
      const id = Math.floor(Math.random() * 100);
      // const newBook={...book}

      // newBook.id = id
      // const books = [...state.books, newBook];

      const books = [...state.books, { ...book, id }];
      return { ...state, books };
    }

    case DELETE: {
      const books = state.books.filter((item) => item.id !== action.payload);
      return { ...state, books };
    }
    case SELECT: {
      return { ...state, selectedBook: action.payload };
    }
    default:
      return state;
  }
};
export default bookReducer;
