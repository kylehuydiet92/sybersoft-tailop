import { DELETE_CART_ITEM } from "../contants/gioHangContants";
const initialState = {
  selectedProduct: null,
  cartList: [],
  productList: [
    {
      id: "sp_1",
      name: "iphoneX",
      price: "30000000",
      screen: "screen_1",
      backCamera: "backCamera_1",
      frontCamera: "frontCamera_1",
      img:
        "https://sudospaces.com/mobilecity-vn/images/2019/12/iphonex-black.jpg",
      desc:
        "iPhone X features a new all-screen design. Face ID, which makes your face your password",
    },
    {
      id: "sp_2",
      name: "Note 7",
      price: "20000000",
      screen: "screen_2",
      backCamera: "backCamera_2",
      frontCamera: "frontCamera_2",
      img:
        "https://www.xtmobile.vn/vnt_upload/product/01_2018/thumbs/600_note_7_blue_600x600.png",
      desc:
        "The Galaxy Note7 comes with a perfectly symmetrical design for good reason",
    },
    {
      id: "sp_3",
      name: "Vivo",
      price: "10000000",
      screen: "screen_3",
      backCamera: "backCamera_3",
      frontCamera: "frontCamera_3",
      img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg",
      desc:
        "A young global smartphone brand focusing on introducing perfect sound quality",
    },
    {
      id: "sp_4",
      name: "Blacberry",
      price: "15000000",
      screen: "screen_4",
      backCamera: "backCamera_4",
      frontCamera: "frontCamera_4",
      img:
        "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg",
      desc:
        "BlackBerry is a line of smartphones, tablets, and services originally designed",
    },
  ],
};
const gioHangReducers = (state = initialState, actions) => {
  switch (actions.type) {
    case "GET_PRODUCT": {
      state.selectedProduct = actions.payload;
      return { ...state };
    }
    case "ADD_TO_CART": {
      // console.log(actions);
      let cartList = [...state.cartList];
      // let { cartList } = state;
      // findindex là pthwc cua es6 cung cap
      /**
       *  tìm thấy trả về index
       * k tim thây ra về -1
       */
      const index = cartList.findIndex((cart) => {
        return cart.id === actions.payload.id;
      });
      // console.log("index:", index);
      if (index !== -1) {
        //tăng số lương

        cartList[index].amount += 1;
      } else {
        //thêm thuộc tính só lượng cho no rồi push vào
        actions.payload.amount = 1;
        cartList.push(actions.payload);
      }

      state.cartList = cartList;
      return { ...state }; //set lại state
    }
    case DELETE_CART_ITEM: {
      // let cartList = [...state.cartList];
      let {
        cartList: [...cartList],
      } = state;
      cartList = cartList.filter((item) => {
        return item.id !== actions.payload.id;
      });
      state.cartList = cartList;
      return { ...state };
    }
    case "CHANGE_AMOUNT_CART": {
      // const { cart, status } = actions.payload;
      let cartList = [...state.cartList];
      const index = cartList.findIndex((item) => {
        return item.id === actions.payload.cart.id;
      });

      if (index !== -1) {
        if (actions.payload.status) {
          // ccoongj
          cartList[index].amount += 1;
        } else {
          //trừ
          if (cartList[index].amount > 1) {
            cartList[index].amount -= 1;
          }
        }
      }

      state.cartList = cartList;
      return { ...state };
    }
    default:
      return state;
  }

  // if (actions.type === "GET_PRODUCT") {
  //   state.selectedProduct = actions.payload;
  //   return { ...state };
  // }
  // if (actions.type === "ADD_TO_CART") {
  //   return { ...state }; //set lại state
  // }
  // return state;
};

export default gioHangReducers;
