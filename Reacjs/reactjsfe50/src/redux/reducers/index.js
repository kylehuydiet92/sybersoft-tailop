import { combineReducers } from "redux";
import gioHangReducers from "./gioHangReducer";
import userReducer from "./userReducer";
import bookReducer from "./bookReducer";
import accountReducer from "./accountReducer";
import roomReducer from "../reducers/roomReducer";

// const initialState = {
//   productList: [],
// };
// const gioHangReducers = (state = initialState) => {
//   return state;
// };

const rootReducer = combineReducers({
  gioHangReducers,
  userReducer,
  bookReducer,
  accountReducer,
  roomReducer,
});

export default rootReducer;
