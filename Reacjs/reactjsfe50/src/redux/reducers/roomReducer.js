import { DELETE, SAVE, SEARCH, SELECT } from "../contants/roomContant";
const initialState = {
  rooms: [
    {
      id: 1,
      soPhong: 1,
      tenPhong: "Phòng 1",
      loaiPhong: "Thường",
      giaPhong: "1500000",
    },
    {
      id: 2,
      soPhong: 2,
      tenPhong: "Phòng 2",
      loaiPhong: "Thường",
      giaPhong: "3000000",
    },
    {
      id: 3,
      soPhong: 3,
      tenPhong: "Phòng 3",
      loaiPhong: "Thường",
      giaPhong: "2500000",
    },
  ],
  search: "",
  selectedRoom: null,
};

const roomReducer = (state = initialState, actions) => {
  switch (actions.type) {
    case SEARCH: {
      return { ...state, search: actions.payload };
    }
    case SAVE: {
      const room = actions.payload;
      if (room.id) {
        // const rooms = state.rooms.map((item) => {
        //   if ((room.id = rooms.id)) {
        //     return room;
        //   }
        //   return item;
        // });

        let rooms = [...state.rooms];
        const index = rooms.findIndex((item) => {
          return room.id === item.id;
        });
        rooms[index] = room;

        return { ...state, rooms };
      }
      const id = Math.floor(Math.random() * 100);
      const rooms = [...state.rooms, { ...room, id }];
      return { ...state, rooms };
    }
    case DELETE: {
      const rooms = state.rooms.filter((item) => item.id !== actions.payload);
      // state.rooms = rooms;
      // return {...state};
      return { ...state, rooms };
    }
    case SELECT: {
      return { ...state, selectedRoom: actions.payload };
    }
    default:
      return state;
  }
};

export default roomReducer;
