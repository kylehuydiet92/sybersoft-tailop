import { SAVE, SELECT, SEARCH, DELETE } from "../contants/userContants";
const initialState = {
  users: [
    {
      id: 1,
      name: "Nguyễn Đức Mạnh",
      username: "manh mạnh",
      email: "kyleguysds@gmail.com",
      phoneNumber: "0338079128",
      type: "USER",
    },

    {
      id: 2,
      name: "Nguyễn Thàng",
      username: "dsads",
      email: "kyleguysds@gmail.com",
      phoneNumber: "0338079128",
      type: "VIP",
    },
  ],
  search: "",
  selectedUser: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH: {
      // dùng cách này sẻ k thây đổi được state và k thể trở về trang
      // thái ban đầu
      //   const users = state.users.filter((user) => {
      //     return user.username.indexOf(action.payload) !== -1;
      //   });

      return { ...state, search: action.payload };
    }
    case SAVE: {
      const user = action.payload;
      if (user.id) {
        // const users = state.users.map((item) => {
        //   if ((user.id = item.id)) {
        //     return user;
        //   }
        //   return item;
        // });

        let users = [...state.users];
        const index = users.findIndex((item) => {
          return user.id === item.id;
        });
        users[index] = user;

        return { ...state, users };
      }

      //nếu k nhẩy vào if => users chưa tồn tại
      const id = Math.floor(Math.random() * 100);
      const users = [...state.users, { ...user, id }];
      return { ...state, users };
    }
    case DELETE: {
      const users = state.users.filter((user) => user.id !== action.payload);
      return { ...state, users };
    }
    case SELECT: {
      return { ...state, selectedUser: action.payload };
    }

    default:
      return state;
  }
};
export default userReducer;
