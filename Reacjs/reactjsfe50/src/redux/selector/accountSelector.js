export const selectAccounts = (state) => {
  const { accounts, search } = state.accountReducer;
  return accounts.filter((book) => {
    return book.taikhoan.indexOf(search) !== -1;
  });
};
