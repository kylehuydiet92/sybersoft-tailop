export const selectBooks = (state) => {
  const { books, search } = state.bookReducer;
  return books.filter((book) => {
    return book.name.indexOf(search) !== -1;
  });
};
