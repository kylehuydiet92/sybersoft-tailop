export const selectRooms = (state) => {
  const { rooms, search } = state.roomReducer;
  return rooms.filter((room) => {
    return room.tenPhong.indexOf(search) !== -1;
  });
};
