export const selectUsers = (state) => {
  const { users, search } = state.userReducer;
  return users.filter((user) => {
    return user.username.indexOf(search) !== -1;
  });
};
